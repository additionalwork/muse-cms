﻿using Cms.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace Cms.App.Tests
{
    [TestClass]
    public class ApplicationDbContextTest
    {
        private readonly ApplicationDbContext appDbContext;
        private DbContextOptions<ApplicationDbContext> _options;
        private Metamodel metamodel;

        public ApplicationDbContextTest()
        {
            var configurationBuilder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false);

            var configuration = configurationBuilder.Build();
            string connectionString = configuration.GetConnectionString("DefaultConnection");

            _options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseNpgsql(connectionString)
                .Options;

            metamodel = new Metamodel();

            appDbContext = new ApplicationDbContext(_options, metamodel);
        }

        [TestMethod, Priority(1)]
        public void CreateCustomTable_Test()
        {
            appDbContext.Database.ExecuteSqlRaw("DROP TABLE IF EXISTS user_myblog;DROP TABLE IF EXISTS user_myupdatedblog");
            var userType = new UserType()
            {
                Name = "myblog",
                Slug = "myblog",
                Fields = new List<Field>()
                {
                    new Field() {Name="Name",Slug="Name"},
                    new Field() {Name="content",Slug="content"},
                    new Field() {Name="author",Slug="author"},
                }
            };
            appDbContext.CreateCustomTable(userType);
            var isCreated = СheckIsCreated(userType);
            Assert.IsTrue(isCreated, "UserType should be created");
            var iscCompl = СheckСompliance(userType);
            Assert.IsTrue(iscCompl, "UserType must match the schema");
        }

        [TestMethod, Priority(2)]
        public void EditCustomTable_Test()
        {
            var userType = appDbContext.FindUserTypeBySlug("myblog");
            userType.Name = "myupdatedblog";
            userType.Slug = "myupdatedblog";
            userType.Fields[0].Name = "newName";
            userType.Fields[0].Slug = "newName";
            userType.Fields[1].Name = "newcontent";
            userType.Fields[1].Slug = "newcontent";
            userType.Fields[1].IsNullable = true;
            userType.Fields[2].Name = "newauthor";
            userType.Fields[2].Slug = "newauthor";
            userType.Fields[2].Type = UserType.BaseTypes[2];
            appDbContext.EditCustomTable("myblog", userType);
            var isEdited = СheckIsCreated(userType);
            Assert.IsTrue(isEdited, "UserType should be edited");
            var iscCompl = СheckСompliance(userType);
            Assert.IsTrue(iscCompl, "UserType must match the schema");
        }

        [TestMethod, Priority(3)]
        public void DeleteCustomTable_Test()
        {
            var userType = appDbContext.FindUserTypeBySlug("myupdatedblog");
            appDbContext.DeleteCustomTable(userType);
            var isDeleted = !СheckIsCreated(userType);
            Assert.IsTrue(isDeleted, "UserType should be deleted");
        }


        public bool СheckIsCreated(UserType userType)
        {
            var sql = @"SELECT table_name FROM information_schema.tables 
                        WHERE table_type = 'BASE TABLE' 
                        AND table_schema NOT IN ('pg_catalog', 'information_schema');";

            var result = appDbContext.Database.SqlQueryRaw<string>(sql, new object[0]).ToList();
            var userTableNames = result.Where(s => s.StartsWith(UserType.UserTablePrefix)).ToList();
            var isCreated = userTableNames.Contains(UserType.UserTablePrefix + userType.Slug);
            return isCreated;
        }

        public bool СheckСompliance(UserType userType)
        {
            var sql = @"SELECT column_name, data_type, character_maximum_length, is_nullable
                        FROM information_schema.columns
                        WHERE table_name = '" + UserType.UserTablePrefix + userType.Slug + "';";

            var result = ExecQuery(appDbContext.Database, sql);
            var dic = userType.GetFieldsDictionary();
            var list = dic.Keys.ToList();
            foreach (DataRow row in result.Rows)
            {
                var columnSlug = (string)row["column_name"];
                if (columnSlug.ToLower() == "id") continue;
                if (dic.ContainsKey(columnSlug))
                {
                    list.Remove(columnSlug);
                }
            }
            foreach (var column_name in list)
            {
                Assert.Fail("the table does not contain field '" + column_name + "'");
            }
            foreach (DataRow row in result.Rows)
            {
                var columnSlug = (string)row["column_name"];
                if (columnSlug.ToLower() == "id") continue;
                var userTypeField = dic[columnSlug];
                var uType = userTypeField.GetDatabaseType().Substring(0, 3);
                var dbType = row["data_type"].ToString().Substring(0, 3);
                //the type names may differ while being the same,
                //for example 'character varying' and 'character varying(256)'
                if (uType != dbType)
                {
                    Assert.Fail("the field '" + columnSlug + "' is of type '" + dbType + "', and should be '" + uType + "'");
                }
                var dbIsNullable = (string)row["is_nullable"] == "YES" ? true : false;
                if (userTypeField.IsNullable != dbIsNullable)
                {
                    Assert.Fail("the field '" + columnSlug + "' IsNullable should be '" + userTypeField.IsNullable + "'");
                }
            }
            return true;
        }

        public DataTable ExecQuery(DatabaseFacade Database,string query)
        {
            DataTable dataTable = new DataTable();
            using (var connection = Database.GetDbConnection())
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = query;
                    using (var reader = command.ExecuteReader())
                    {
                        dataTable.Load(reader);
                    }
                }
            }
            return dataTable;
        }

    }
}
