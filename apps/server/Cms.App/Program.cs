using System.Reflection;
using Cms.Areas.Identity;
using Cms.Data;
using Cms.Services;
using MatBlazor;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Text.RegularExpressions;

if (args.Contains("-v"))
{
    var info = Assembly.GetEntryAssembly()?.GetName().Version.ToString();
    if (info == null)
    {
        Console.WriteLine();
        Environment.Exit(1);
    }

    Console.Write(info);
    Environment.Exit(0);
}

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowAll", builder =>
    {
        builder.AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader();
    });
});
// Add services to the container.
builder.Services.AddSingleton<Metamodel>();
builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection"))
        .ReplaceService<IModelCacheKeyFactory, MetamodelAwareCacheKeyFactory>(),ServiceLifetime.Transient
);
builder.Services.AddDatabaseDeveloperPageExceptionFilter();
builder.Services.AddDefaultIdentity<AppUser>(options => options.SignIn.RequireConfirmedAccount = false)
    .AddRoles<IdentityRole>()
    .AddEntityFrameworkStores<ApplicationDbContext>()
    .AddTokenProvider("MyApp", typeof(DataProtectorTokenProvider<AppUser>));

builder.Services.AddAuthentication()
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new AuthOptions().MyTokenValidationParameters();
    });
builder.Services.AddScoped<JsonFileService>();
builder.Services.AddScoped<UserTypeService>();
builder.Services.AddScoped<AuthService>();
builder.Services.AddScoped<SiteService>();
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddMatBlazor();
builder.Services.AddScoped<AuthenticationStateProvider, RevalidatingIdentityAuthenticationStateProvider<AppUser>>();

builder.Services.AddHttpClient();
builder.Services.AddControllers();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v0", new Microsoft.OpenApi.Models.OpenApiInfo
    {
        Title = "CMS API",
        Version = "v0",
    });
    c.TagActionsBy(api => new[]
    {
        Regex.Match(api.ActionDescriptor.DisplayName, @"\.(\w+)Controller").Groups[1].Value,
    });
    c.UseAllOfForInheritance();
    c.UseOneOfForPolymorphism();
    c.CustomOperationIds(
        api => Regex.Match(api.ActionDescriptor.DisplayName, @"\.(\w+) \(").Groups[1].Value);
    c.CustomSchemaIds(x =>
    {
        var name = x.ShortDisplayName() ?? throw new Exception("No null allowed");

        if (name.Contains("PaginatedList"))
        {
            var tDto = Regex.Match(name, @"<(\w+)>").Groups[1].Value;
            name = $"PaginatedListOf{tDto}";
        }

        name = name.EndsWith("Dto") ? name.Remove(name.Length - 3) : name;
        return name;
    });
});

builder.Services.AddCors();

var app = builder.Build();

app.UseCors(builder =>
{
    builder
        .AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader();
});

// Configure the HTTP request pipeline.
if (!app.Environment.IsProduction())
{
    app.UseMigrationsEndPoint();
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v0/swagger.json", "CMS API v0");
    });
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.Map("/public", health =>
{
    app.UseStaticFiles(new StaticFileOptions()
    {
        HttpsCompression = Microsoft.AspNetCore.Http.Features.HttpsCompressionMode.Compress,
        OnPrepareResponse = (context) =>
        {
            var headers = context.Context.Response.GetTypedHeaders();
            headers.CacheControl = new Microsoft.Net.Http.Headers.CacheControlHeaderValue
            {
                Public = true,
                MaxAge = TimeSpan.FromDays(365),
            };
        },
    });
});

app.UseStaticFiles();
app.UseRouting();
app.UseAuthorization();
app.UseCors("AllowAll");
app.MapControllers();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");
// update Scheme from db
// new dbcontext will can get user types
using (var scope = app.Services.CreateScope())
{
    var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
    dbContext.ChangeScheme();
}

app.Run();