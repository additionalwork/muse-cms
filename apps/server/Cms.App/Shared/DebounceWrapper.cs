namespace Cms.Shared
{
    using System.Timers;

    public class DebounceWrapper
    {
        private readonly int DebounceTime;
        private readonly Action Callback;
        private Timer Timer;

        public DebounceWrapper(Action action, int time)
        {
            DebounceTime = time;
            Callback = action;
            Timer = new(DebounceTime);
        }

        ~DebounceWrapper()
        {
            Timer.Dispose();
        }

        private void TimerElapsedHandler(object? sender, EventArgs e)
        {
            Timer.Dispose();
            Callback();
        }

        public void Run()
        {
            Timer.Dispose();
            Timer = new(DebounceTime);
            Timer.Elapsed += TimerElapsedHandler;
            Timer.Enabled = true;
            Timer.Start();
        }
    }
}