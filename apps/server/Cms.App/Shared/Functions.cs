using Cms.Data;

namespace Cms.Shared
{
    public static class Extension
    {
        public static string GetAddName(this Field field)
        {
            if (UserType.BaseTypes.Contains(field.Type) && (!field.IsArray || field.Type != UserType.BaseTypes[3]))
            {
                return "Добавление";
            } else
            {
                return "Выберите";
            }
        }
    }
}