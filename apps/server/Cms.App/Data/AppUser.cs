using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Cms.Data
{
    public class AppUser : IdentityUser
    {
        public virtual List<Site> Sites { get; set; } = new List<Site>();
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [NotMapped]
        public string? Role { get; set; }
    }
}
