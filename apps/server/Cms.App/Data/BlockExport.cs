using System.Text.Json;

namespace Cms.Data
{
    public class BlockExport
    {
        public int? id { get; set; }
        public string? tagName { get; set; }
        public List<BlockExport> childNodes { get; set; }
        public string? text { get; set; }
        public Dictionary<string,string>? attributes { get; set; }

        public BlockExport(Block block, List<BlockExport> childNodes)
        {
            this.id = block.id;
            this.tagName = block.tagName;
            this.childNodes = childNodes;
            this.text = block.text;
            if(block.attributes == null)
            {
                this.attributes = null;
            }
            else
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                foreach (string[] pair in JsonSerializer.Deserialize<string[][]>(block.attributes))
                {
                    dictionary[pair[0]] = pair[1];
                }
                this.attributes = dictionary;
            }
            
        }
    }
}