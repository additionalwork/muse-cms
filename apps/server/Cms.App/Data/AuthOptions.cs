﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Cms.Data
{
    public class AuthOptions
    {
        public string Issuer = "myissuer";
        public string Audience = "myaudience";
        public string SecretKey = "my_very_long_secret_key";
        public string SecretKeyRefresh = "my_another_long_secret_key";
        public int ExpirationDays = 1;
        public int ExpirationMins = 1;
        public AuthOptions() { }
        public TokenValidationParameters MyTokenValidationParameters()
        {
            return new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = Issuer,
                ValidAudience = Audience,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecretKey)),
                ClockSkew = TimeSpan.Zero,
            };
        }


        public TokenValidationParameters MyRefreshTokenValidationParameters()
        {
            return new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateIssuerSigningKey = true,
                ValidateLifetime = true,
                ValidIssuer = Issuer,
                ValidAudience = Audience,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecretKeyRefresh)),
                ClockSkew = TimeSpan.Zero,
            };
        }
    }
}
