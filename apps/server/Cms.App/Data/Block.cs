using System.ComponentModel.DataAnnotations;

namespace Cms.Data
{
    public class Block
    {
        [Key]
        public int? id { get; set; }
        [Required]
        public string tagName { get; set; }
        public int? parent { get; set; }
        public string? text { get; set; }
        public string? attributes { get; set; }
        public int? pageId { get; set; }
        public int? nextBlockId { get; set; }

        public Block(int _id, string tagName, int? parent, string text, string attributes, int? nextBlockId)
        {
            this.id = _id;
            this.tagName = tagName;
            this.parent = parent;
            this.text = text;
            this.attributes = attributes;
            this.nextBlockId = nextBlockId;
        }
        public Block(Block b)
        {
            this.id = b.id;
            this.tagName = b.tagName;
            this.parent = b.parent;
            this.text = b.text;
            this.attributes = b.attributes;
            this.nextBlockId = b.nextBlockId;
            this.pageId = b.pageId;
        }
        public Block()
        {
        }
        public void CloneTo(Block b)
        {
            b.id = this.id;
            b.tagName = this.tagName;
            b.parent = this.parent;
            b.text = this.text;
            b.attributes = this.attributes;
            b.nextBlockId = this.nextBlockId;
            b.pageId = this.pageId;
        }
    }
}