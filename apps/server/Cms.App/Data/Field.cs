using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cms.Data
{
    public class Field
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; } = "";

        public string? Description { get; set; }

        [Required]
        [RegularExpression(@"^(?!.*\b(integer|text|boolean|file)\b)[a-zA-Z\-_]*$", ErrorMessage = "Slug can contain only Latin letters and must not be one of the keywords")]
        public string Slug { get; set; } = "";

        [Required]
        public string Type { get; set; }  = "text";

        public int Order { get; set; } = 0;

        public bool IsNullable { get; set; } = false;

        [Required]
        public bool IsShownInList { get; set; } = false;

        [Required]
        public bool IsArray { get; set; } = false;

        [NotMapped]
        public object? Value { get; set; } = "";

        [NotMapped] public Dictionary<string, object> LocalizeDictionary { get; set; } = new() {{"", ""}};

        public bool Localized { get; set; } = false;

        public void CloneTo(Field field)
        {
            field.Id = Id;
            field.Name = Name;
            field.Description = Description;
            field.Slug = Slug;
            field.Type = Type;
            field.IsShownInList = IsShownInList;
            field.IsNullable = IsNullable;
            field.IsArray = IsArray;
            field.Value = Value;
            field.Order = Order;
            field.Localized = Localized;
            field.LocalizeDictionary = LocalizeDictionary;
        }

        public Type GetFieldType()
        {
            if (IsArray) return typeof(string);
            switch (Type.ToLower())
            {
                case "integer":
                    return typeof(int?);
                case "text":
                    return typeof(string);
                case "boolean":
                    return typeof(bool?);
                case "file":
                    return typeof(string);
                default:
                    return typeof(string);
            }
        }

        public object? GetFieldDefaultValue()
        {
            if (IsNullable) return null;
            if (IsArray) return "[]";
            switch (Type.ToLower())
            {
                case "integer":
                    return 0;
                case "text":
                    return "";
                case "boolean":
                    return false;
                case "file":
                    return "";
                default:
                    return "";
            }
        }

        public string GetDatabaseType()
        {
            if (IsArray) return "jsonb";
            switch (Type.ToLower())
            {
                case "integer":
                    return "int";
                case "text":
                    return "character varying(256)";
                case "boolean":
                    return "boolean";
                case "file":
                    return "character varying(256)";
                default:
                    return "character varying(256)";
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is Field other)
            {
                return Slug == other.Slug
                    && Type == other.Type
                    && IsNullable == other.IsNullable
                    && IsArray == other.IsArray
                    && Localized == other.Localized
                    && Value == other.Value;
            }
            return false;
        }
    }
}
