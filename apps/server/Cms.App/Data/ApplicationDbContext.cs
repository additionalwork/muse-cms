﻿using Cms.Migrations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using System.Data;
using System.Text.Json;

namespace Cms.Data
{
    public class ApplicationDbContext : IdentityDbContext<AppUser>, IMetamodelAccessor
    {
        public DbSet<Page> Pages { get; set; }
        public DbSet<Block> Blocks { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<Field> Fields { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<UserTypeInstance> UserTypeInstances { get; set; }
        public Metamodel Metamodel { get; }

        readonly DbContextOptions<ApplicationDbContext> dboptions;
        public static event EventHandler UserTypeChanged;

        public static event EventHandler SiteChanged;

        public static readonly List<string> LanguageList = new() { "En", "Fr", "De", "Zh" };

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, Metamodel metamodel) : base(options)
        {
            dboptions = options;
            Metamodel = metamodel;
        }
        
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseNpgsql(
                    o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery));
        }

        public ApplicationDbContext ReCreate()
        {
            return new ApplicationDbContext(dboptions, Metamodel);
        }

        public void OnUserTypeChanged()
        {
            UserTypeChanged?.Invoke(this, EventArgs.Empty);
        }

        public void OnSiteChanged()
        {
            SiteChanged?.Invoke(this, EventArgs.Empty);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UserType>()
                .HasMany(u => u.Fields)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);
            Metamodel.ApplyChanges(modelBuilder);
        }

        public void CreateCustomTable(UserType table)
        {
            var customTable = new CustomTable
            {
                table = table
            };
            var migrationScript = GenerateSqlMigration(customTable, MigrationOperationType.Up);
            Database.ExecuteSqlRaw(migrationScript);

            UserTypes.Add(table);
            SaveChanges();
            OnUserTypeChanged();
            ChangeScheme();
        }

        public void EditCustomTable(UserType table)
        {
            var oldType = FirstUserTypes(table.Id);
            var toRemove = new List<Field>();
            var toUpdate = new Dictionary<Field, Field>();

            foreach (var field in oldType.Fields)
            {
                var search = table.Fields.FirstOrDefault(f => f.Id == field.Id);
                if (search != null)
                {
                    if (!search.Equals(field))
                        toUpdate.Add(field, search);
                }
                else
                {
                    toRemove.Add(field);
                }
            }

            var fieldToDelSlugs = toRemove.Select(f => f.Slug).ToList();
            var fieldToDelLocalizeSlugs = new List<string>();
            foreach (var pair in toUpdate)
            {
                if ((pair.Key.IsArray && !pair.Value.IsArray) ||
                    (!pair.Key.IsArray && pair.Value.IsArray) ||
                  (pair.Key.Type == "file" && pair.Value.Type != "file"))
                    fieldToDelSlugs.Add(pair.Key.Slug);
                else if(pair.Key.Localized && !pair.Value.Localized)
                    fieldToDelLocalizeSlugs.Add(pair.Key.Slug);
            }
            RemoveUnusedFiles(oldType, fieldToDelSlugs,false);
            RemoveUnusedFiles(oldType, fieldToDelLocalizeSlugs, true);

            var toAdd = table.Fields.Where(field => field.Id == Guid.Empty).ToList();
            var customTable = new CustomTable
            {
                table = table,
                oldTable = oldType,
                toRemove = toRemove,
                toUpdate = toUpdate,
                toAdd = toAdd,
            };

            var migrationScript = GenerateSqlMigration(customTable, MigrationOperationType.Up);
            Database.ExecuteSqlRaw(migrationScript);

            table.CloneTo(oldType);
            oldType.Fields.ForEach(f => f.Id = Guid.Empty);
            oldType.Sites = new List<Site>();
            oldType.Sites.AddRange(table.Sites
                .Select((site) =>
                {
                    return Sites.FirstOrDefault((s) => s.Id == site.Id);
                }));
            UserTypes.Update(oldType);
            SaveChanges();
            Fields.Where(p => EF.Property<Guid?>(p, "UserTypeId") == null).ExecuteDelete();
            OnUserTypeChanged();
            ChangeScheme();
        }

        public void DeleteCustomTable(UserType table)
        {
            RemoveUnusedFiles(table);
            var customTable = new CustomTable
            {
                table = table
            };
            var migrationScript = GenerateSqlMigration(customTable, MigrationOperationType.Down);
            Database.ExecuteSqlRaw(migrationScript);
            var sites = Sites
                .Include(site => site.UserTypes)
                .Where(site => site.UserTypes.FirstOrDefault(type => type.Id == table.Id) != null).ToList();
            sites.ForEach(site =>
            {
                var type = site.UserTypes.FirstOrDefault(type => type.Id == table.Id);
                if (type is not null)
                {
                    site.UserTypes.Remove(type);
                }
            });

            UserTypes.Remove(table);
            SaveChanges();

            RemoveUnusedLinkFields(table);
            OnUserTypeChanged();
            ChangeScheme();
        }

        public UserType? FindUserTypeBySlug(string slug)
        {
            var userType = UserTypes.Include(u => u.Fields).FirstOrDefault(e => e.Slug == slug);
            userType.Fields = userType.Fields.OrderBy(f => f.Order).ToList();
            return userType;
        }

        public List<UserType> GetUserTypes()
        {
            return UserTypes.Include(u => u.Fields).ToList();
        }

        public List<UserType>? GetSiteUserTypes(string domain)
        {
            return Sites
                .FromSql($"SELECT * FROM \"Sites\" WHERE {domain}=ANY(LOWER(\"Domains\"::text)::text[]) LIMIT 1")
                .Include((site) => site.UserTypes)
                .ThenInclude((userType) => userType.Fields)
                .FirstOrDefault((site) => site != null)?
                .UserTypes.ToList();
        }

        public UserType FirstUserTypes(Guid id)
        {
            return UserTypes.Include(u => u.Fields).First(u => u.Id == id);
        }

        private string GenerateSqlMigration(CustomTable table, MigrationOperationType op)
        {
            IReadOnlyList<MigrationOperation>? operation = null;
            switch (op)
            {
                case MigrationOperationType.Up:
                    operation = table.UpOperations;
                    break;
                case MigrationOperationType.Down:
                    operation = table.DownOperations;
                    break;
            }
            var migrationSqlGenerator = this.GetInfrastructure().GetService<IMigrationsSqlGenerator>();
            var sqlStatements = migrationSqlGenerator.Generate(operation);

            return string.Join("\n\n", sqlStatements.Select(s => s.CommandText));
        }

        public List<UserTypeInstance> GetUserTypeInstances(UserType Scheme, Site? site = null)
        {
            var instances = GetUserTypeInstancesRows(Scheme, site);
            var list = new List<UserTypeInstance>();
            foreach (var row in instances)
            {
                var dic = Scheme.GetFieldsDictionary();
                var inst = new UserTypeInstance((Guid)row["Id"], row.ContainsKey("Name") ? (string)row["Name"] : "Instance");
                foreach (var key in row.Keys)
                {
                    if (key == "Id") continue;
                    dic[key].Value = row[key];
                }
                inst.Fields = dic.Values.ToList();
                list.Add(inst);
            }
            return list;
        }

        public List<Dictionary<string, object>> GetUserTypeInstancesRows(UserType Scheme, Site? site, string lang = CustomTable.DefaultLanguage)
        {
            var tableName = UserType.UserTablePrefix + Scheme.Slug;
            var ids = site?.UserTypeInstances.Select((instance) => instance.Id.ToString()).ToList();
            var instances = site == null ? Set<Dictionary<string, object>>(tableName).ToList()
                : Set<Dictionary<string, object>>(tableName).Where((item) => ids.Contains(item["Id"].ToString())).ToList();
            object temp;

            if (!Scheme.Localized) return instances;
            
            var localizedTableName = tableName + "_"+lang;
            var localizedInstances = site == null ? Set<Dictionary<string, object>>(localizedTableName).ToList()
                : Set<Dictionary<string, object>>(localizedTableName)
                    .Where((item) => ids.Contains(item["Id"].ToString())).ToList();
            
            localizedInstances.ForEach(localizedInstance =>
            {
                localizedInstance.ToList().ForEach(el =>
                {
                    var index = instances.FindIndex(dict => dict["Id"].ToString() == localizedInstance["Id"].ToString());
                    
                    
                    if (index != -1 && el.Key != "Id" && !instances[index].TryGetValue(el.Key, out temp))
                    {
                        instances[index].Add(el.Key, el.Value);
                    }
                });
            });
            return instances;
        }

        public Dictionary<string, Dictionary<string, object>> getUserTypeInstancesRowsDic(UserType Scheme, Site? site, string lang = CustomTable.DefaultLanguage)
        {
            var res = GetUserTypeInstancesRows(Scheme, site, lang);
            return res.ToDictionary(x => x[x.Keys.First(y => y == "Id")].ToString());
        }

        public UserTypeInstance GetUserTypeInstance(UserType scheme, Guid id)
        {
            var tableName = UserType.UserTablePrefix + scheme.Slug;
            var fieldsDictionary = scheme.GetFieldsDictionary();

            var dbSet = Set<Dictionary<string, object>>(tableName);
            var defaultInstanceDict = dbSet.Single(p => EF.Property<Guid>(p, "Id") == id);

            foreach (var key in defaultInstanceDict.Keys.Where(key => key != "Id"))
            {
                fieldsDictionary[key].Value = defaultInstanceDict[key];
            }

            if (scheme.Localized)
            {
                var localizedTableName = tableName + "_Ru";
                var localizedDbSet = Set<Dictionary<string, object>>(localizedTableName);
                var localizedInstanceDict = localizedDbSet.Single(p => EF.Property<Guid>(p, "Id") == id);

                foreach (var key in localizedInstanceDict.Keys.Where(key => key != "Id"))
                {
                    var dict = new Dictionary<string, object>();
                    LanguageList.ForEach(el =>
                    {
                        var localizedTableName = tableName + "_" + el;
                        var localizedDbSet = Set<Dictionary<string, object>>(localizedTableName);
                        var localizedInstanceDict = localizedDbSet.Single(p => EF.Property<Guid>(p, "Id") == id);

                        if (localizedInstanceDict.ContainsKey(key) && 
                        localizedInstanceDict[key] != null &&
                        localizedInstanceDict[key] != "")
                            dict.Add(el, localizedInstanceDict[key]);
                    });

                    fieldsDictionary[key].Value = localizedInstanceDict[key];
                    fieldsDictionary[key].LocalizeDictionary = dict;
                }
            }

            var inst = new UserTypeInstance((Guid)defaultInstanceDict["Id"], (defaultInstanceDict.ContainsKey("Name") ? (string)defaultInstanceDict["Name"] : "Instance") + "#" + defaultInstanceDict["Id"])
            {
                Fields = fieldsDictionary.Values.ToList()
            };
            return inst;
        }

        public Dictionary<string, object> CreateUserTypeInstance(UserType scheme, UserTypeInstance instance, Site? site)
        {
            var fieldsDictionary = instance.GetFieldsDictionary();
            var tableName = UserType.UserTablePrefix + scheme.Slug;
            var id = Guid.NewGuid();

            var defaultFieldsDictionary = fieldsDictionary.Where(x => !x.Value.Localized).ToDictionary(i => i.Key, i => i.Value);
            var defaultInstanceDictionary = defaultFieldsDictionary.Keys.ToDictionary(key => key, key => fieldsDictionary[key].Value);
            defaultInstanceDictionary.Add("Id", id);


            var dbSet = Set<Dictionary<string, object>>(tableName);

            dbSet.Add(defaultInstanceDictionary);

            if (scheme.Localized)
            {
                var enumerableLocalizedFieldsDict = fieldsDictionary.Where(x => x.Value.Localized);

                foreach (var prefix in LanguageList)
                {
                    var otherLocalizedTableName = tableName + "_" + prefix;

                    var dictionary = enumerableLocalizedFieldsDict.ToDictionary(i => i.Key, i => i.Value);

                    var fieldsWithPrefix = dictionary.Where(el =>
                        el.Value.LocalizeDictionary != null & el.Value.LocalizeDictionary.ContainsKey(prefix)).ToDictionary(i => i.Key, i => i.Value);

                    var fieldsWithPrefixDictionary = fieldsWithPrefix.Keys.ToDictionary(key => key, key => fieldsDictionary[key].LocalizeDictionary[prefix]);
                    fieldsWithPrefixDictionary.Add("Id", id);

                    var dbSetLocalizedOther = Set<Dictionary<string, object>>(otherLocalizedTableName);
                    dbSetLocalizedOther.Add(fieldsWithPrefixDictionary);
                }

                var localizedTableName = tableName + "_Ru";
                var localizedFieldsDictionary = fieldsDictionary.Where(x => x.Value.Localized).ToDictionary(i => i.Key, i => i.Value);
                var localizedInstanceDictionary = localizedFieldsDictionary.Keys.ToDictionary(key => key, key => fieldsDictionary[key].Value);

                localizedInstanceDictionary.Add("Id", id);

                localizedInstanceDictionary.ToList().ForEach(el =>
                {
                    if (el.Key != "Id")
                    {
                        defaultInstanceDictionary.Add(el.Key, el.Value);
                    }
                });

                var dbSetLocalized = Set<Dictionary<string, object>>(localizedTableName);
                dbSetLocalized.Add(localizedInstanceDictionary);
            }

            if (site is not null)
            {
                var parseRes = defaultInstanceDictionary.TryGetValue("Name", out object? name);
                var siteRes = Sites.FirstOrDefault((s) => s.Id == site.Id);

                if (!parseRes || name?.ToString() == string.Empty) throw new ArgumentException("Invalid name of user type instance");
                if (siteRes is null) throw new ArgumentException("There is no site with such id");

                var instanceToAdd = new UserTypeInstance(id, name?.ToString());
                UserTypeInstances.Add(instanceToAdd);
                siteRes.UserTypeInstances.Add(instanceToAdd);
            }

            SaveChanges();
            return defaultInstanceDictionary;
        }

        public void UpdateUserTypeInstance(UserType scheme, UserTypeInstance instance)
        {
            var fieldsDictionary = instance.GetFieldsDictionary();
            var tableName = UserType.UserTablePrefix + scheme.Slug;

            var dbSet = Set<Dictionary<string, object>>(tableName);
            var defaultFieldsDictionary = fieldsDictionary.Where(x => !x.Value.Localized).ToDictionary(i => i.Key, i => i.Value);
            var defaultInstancesDict = dbSet.Single(p => EF.Property<Guid>(p, "Id") == instance.Id);

            foreach (var key in defaultFieldsDictionary.Keys.Where(key => key != "Id"))
            {
                defaultInstancesDict[key] = fieldsDictionary[key].Value;
            }
            dbSet.Update(defaultInstancesDict);

            if (scheme.Localized)
            {
                var enumerableLocalizedFieldsDict = fieldsDictionary.Where(x => x.Value.Localized);

                foreach (var prefix in LanguageList)
                {
                    var otherLocalizedTableName = tableName + "_" + prefix;
                    var otherDbSetLocalized = Set<Dictionary<string, object>>(otherLocalizedTableName);
                    var otherLocalizedFieldsDictionary = enumerableLocalizedFieldsDict.ToDictionary(i => i.Key, i => i.Value);
                    var otherLocalizedInstanceDict = otherDbSetLocalized.Single(p => EF.Property<Guid>(p, "Id") == instance.Id);

                    foreach (var key in otherLocalizedFieldsDictionary.Keys.Where(key => key != "Id"))
                    {
                        if(fieldsDictionary[key].LocalizeDictionary.ContainsKey(prefix))
                            otherLocalizedInstanceDict[key] = fieldsDictionary[key].LocalizeDictionary[prefix];
                    }
                }

                var localizedTableName = tableName + "_Ru";

                var dbSetLocalized = Set<Dictionary<string, object>>(localizedTableName);
                var localizedFieldsDictionary = enumerableLocalizedFieldsDict.ToDictionary(i => i.Key, i => i.Value);
                var localizedInstancesDict = dbSetLocalized.Single(p => EF.Property<Guid>(p, "Id") == instance.Id);

                foreach (var key in localizedFieldsDictionary.Keys.Where(key => key != "Id"))
                {
                    localizedInstancesDict[key] = fieldsDictionary[key].Value;
                }
                dbSetLocalized.Update(localizedInstancesDict);
            }

            SaveChanges();
        }

        public void RemoveUserTypeInstance(UserType scheme, UserTypeInstance inst)
        {
            var id = inst.Id;
            var tableName = UserType.UserTablePrefix + scheme.Slug;
            var dbSet = Set<Dictionary<string, object>>(tableName);
            dbSet.Where(p => EF.Property<Guid>(p, "Id") == id).ExecuteDelete();

            var removed = UserTypeInstances.FirstOrDefault((instance) => instance.Id == id);
            if (removed is not null)
            {
                UserTypeInstances.Remove(removed);
                SaveChanges();
            }

            if (scheme.Localized)
            {
                var localizedTableName = tableName + "_Ru";
                var localizedDbSet = Set<Dictionary<string, object>>(localizedTableName);
                localizedDbSet.Where(p => EF.Property<Guid>(p, "Id") == id).ExecuteDelete();
            }

            RemoveUnusedFilesInst(inst, scheme.Fields.Select(f => f.Slug).ToList(), false);
            RemoveUnusedLinks(scheme, id);
            SaveChanges();
        }

        public void RemoveUserTypeInstance(UserType scheme, Guid id, Site site)
        {
            var siteRes = Sites.Include((site) => site.UserTypeInstances).FirstOrDefault((s) => s.Id == site.Id);
            var instanceToRemove = siteRes.UserTypeInstances.FirstOrDefault((inst) => inst.Id == id);
            siteRes.UserTypeInstances.Remove(instanceToRemove);
            SaveChanges();

            var sitesContainingCount = Sites
                .Where((site) => site.UserTypeInstances.FirstOrDefault((instance) => instance.Id == id) != null).Count();
            if (sitesContainingCount > 0)
            {
                return;
            }

            var removed = UserTypeInstances.FirstOrDefault((instance) => instance.Id == id);
            if (removed is not null)
            {
                UserTypeInstances.Remove(removed);
            }

            var tableName = UserType.UserTablePrefix + scheme.Slug;
            var dbSet = Set<Dictionary<string, object>>(tableName);
            dbSet.Where(p => EF.Property<Guid>(p, "Id") == id).ExecuteDelete();

            if (scheme.Localized)
            {
                var localizedTableName = tableName + "_Ru";
                var localizedDbSet = Set<Dictionary<string, object>>(localizedTableName);
                localizedDbSet.Where(p => EF.Property<Guid>(p, "Id") == id).ExecuteDelete();
            }

            RemoveUnusedFilesInst(instanceToRemove, scheme.Fields.Select(f => f.Slug).ToList(), false);
            RemoveUnusedLinks(scheme, id);
            SaveChanges();
        }

        private void RemoveUnusedFiles(UserType userType)
        {
            RemoveUnusedFiles(userType, userType.Fields.Select(f => f.Slug).ToList(), false);
        }

        private void RemoveUnusedFiles(UserType userType, List<string> fieldToDelSlugs, bool onlyDelLocalization)
        {
            if (fieldToDelSlugs.Count == 0) return;
            var tableName = UserType.UserTablePrefix + userType.Slug;
            var instanceIds = Set<Dictionary<string, object>>(tableName).Select(i =>new Guid(i["Id"].ToString())).ToList();
            foreach (var id in instanceIds)
            {
                var inst = GetUserTypeInstance(userType, id);
                RemoveUnusedFilesInst(inst, fieldToDelSlugs, onlyDelLocalization);
            }
        }

        private void RemoveUnusedFilesInst(UserTypeInstance inst, List<string> fieldToDelSlugs, bool onlyDelLocalization)
        {
            foreach (var field in inst.Fields)
            {
                if(field.Type == UserType.BaseTypes[3] && field.Value != null && fieldToDelSlugs.Contains(field.Slug))
                {
                    if (!field.IsArray)
                    {
                        if(!onlyDelLocalization)
                            Cms.Services.UploadFileService.DeleteFile((string)field.Value);
                        if (field.LocalizeDictionary != null)
                            foreach (var pair in field.LocalizeDictionary)
                            {
                                if (pair.Value == "") continue;
                                Cms.Services.UploadFileService.DeleteFile((string)pair.Value);
                            }
                    }
                    else
                    {
                        var toRemoveFilesList = onlyDelLocalization?new List<string>():JsonSerializer.Deserialize<List<string>>(field.Value.ToString());
                        if (field.LocalizeDictionary != null)
                            foreach (var pair in field.LocalizeDictionary)
                            {
                                if (pair.Value == "") continue;
                                var toRemove = JsonSerializer.Deserialize<List<string>>(pair.Value.ToString());
                                toRemoveFilesList.AddRange(toRemove);
                            }
                        foreach (var file in toRemoveFilesList)
                        {
                            Cms.Services.UploadFileService.DeleteFile(file);
                        }
                    }
                }
            }
        }
        private void RemoveUnusedLinks(UserType scheme, Guid id)
        {
            var stringId = id.ToString();
            var userTypes = UserTypes.Include(u => u.Fields).ToList();

            foreach (var userType in userTypes)
            {
                var fieldsToClean = userType.Fields.Where(x => x.Type == scheme.Slug).ToList();

                if (fieldsToClean.Count > 0)
                {
                    var dbSet = Set<Dictionary<string, object>>(UserType.UserTablePrefix + userType.Slug).ToList();
                    foreach (var row in dbSet)
                    {
                        foreach (var field in fieldsToClean)
                        {
                            if (row[field.Slug] is not null && row[field.Slug].ToString().Length > 0)
                            {
                                if (row[field.Slug].ToString().StartsWith("["))
                                {
                                    var list = JsonSerializer.Deserialize<List<string>>(row[field.Slug].ToString());
                                    list.RemoveAll(s => s == stringId);
                                    row[field.Slug] = JsonSerializer.Serialize(list);
                                } else if (row[field.Slug].ToString() == stringId)
                                {
                                    row[field.Slug] = string.Empty;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void RemoveUnusedLinkFields(UserType scheme)
        {
            var userTypes = UserTypes.Include(u => u.Fields).ToList();
            foreach (var userType in userTypes)
            {
                List<Field> fieldsToClean = userType.Fields.Where(x => x.Type == scheme.Slug).ToList();
                if (fieldsToClean.Count == 0)
                    continue;
                UserType temp = new UserType();
                userType.CloneTo(temp);
                foreach (var item in fieldsToClean)
                {
                    temp.Fields.Remove(item);
                }
                EditCustomTable(temp);
            }
        }

        public bool ExistLinkFields(UserType scheme)
        {
            var userTypes = UserTypes.Include(u => u.Fields).ToList();
            foreach (var userType in userTypes)
            {
                if (userType.Fields.Count(x => x.Type == scheme.Slug) > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public void ChangeScheme()
        {
            var userTypes = UserTypes.Include(u => u.Fields).ToList();
            Metamodel.Version++;

            var id = new AdditionalField
            {
                PropertyName = "Id",
                PropertyType = typeof(Guid),
                IsRequired = true
            };
            Metamodel.Entities = new List<AdditionalEntity>();
            foreach (var userType in userTypes)
            {
                var tableName = UserType.UserTablePrefix + userType.Slug;
                var defaultFields = new List<AdditionalField> { id };
                defaultFields.AddRange(userType.Fields.FindAll(field => !field.Localized)
                    .Select(field => new AdditionalField
                    {
                        PropertyName = field.Slug,
                        PropertyType = field.GetFieldType(),
                        DataBaseColumnType = field.GetDatabaseType()
                    }));

                if (userType.Localized)
                {
                    var localizedField = new List<AdditionalField> { id };
                    var localizedTableName = tableName + "_Ru";
                    localizedField.AddRange(userType.Fields.FindAll(field => field.Localized)
                        .Select(field => new AdditionalField
                        {
                            PropertyName = field.Slug,
                            PropertyType = field.GetFieldType(),
                            DataBaseColumnType = field.GetDatabaseType()
                        }));

                    Metamodel.Entities.Add(new AdditionalEntity
                    {
                        EntityName = localizedTableName,
                        TableName = localizedTableName,
                        Key = { id },
                        Fields = localizedField
                    });

                    LanguageList.ForEach(el =>
                    {
                        var localizedTableName = tableName + "_" + el;
                        localizedField.AddRange(userType.Fields.FindAll(field => field.Localized)
                            .Select(field => new AdditionalField
                            {
                                PropertyName = field.Slug,
                                PropertyType = field.GetFieldType(),
                                DataBaseColumnType = field.GetDatabaseType()
                            }));

                        Metamodel.Entities.Add(new AdditionalEntity
                        {
                            EntityName = localizedTableName,
                            TableName = localizedTableName,
                            Key = { id },
                            Fields = localizedField
                        });
                    });
                }
                Metamodel.Entities.Add(new AdditionalEntity
                {
                    EntityName = tableName,
                    TableName = tableName,
                    Key = { id },
                    Fields = defaultFields
                });
            }
        }

        public void AddToUserTypeInstances(List<UserTypeInstance> list)
        {
            foreach (var instance in list)
            {
                var res = UserTypeInstances.Contains(instance);
                if (!res)
                {
                    UserTypeInstances.Add(instance);
                }
            }
        }

        public void RemoveFromUserTypeInstances(List<UserTypeInstance> list)
        {
            foreach (var instance in list)
            {
                var sitesContainingCount = Sites
                    .AsNoTracking().Count(site => site.UserTypeInstances.FirstOrDefault((inst) => inst.Id == instance.Id) != null);
                if (sitesContainingCount < 2)
                {
                    var instanceToRemove = UserTypeInstances.FirstOrDefault((inst) => inst.Id == instance.Id);
                    if (instanceToRemove is null) continue;
                    UserTypeInstances.Remove(instanceToRemove);
                }
            }
        }
    }
    public enum MigrationOperationType
    {
        Up,
        Down
    }
}