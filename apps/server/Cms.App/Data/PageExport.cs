namespace Cms.Data
{
    public class PageExport
    {
        public int? id { get; set; }
        public string? name { get; set; }
        public string? description { get; set; }
        public string? path { get; set; }
        public List<BlockExport> blocks { get; set; }
        public bool? requiresAuth { get; set; }

        public PageExport(Page page,string? path, List<BlockExport> blocks)
        {
            this.id = page.id;
            this.name = page.name;
            this.description = page.description;
            this.requiresAuth = page.requiresAuth;
            this.path = path;
            this.blocks = blocks;
        }
    }
}