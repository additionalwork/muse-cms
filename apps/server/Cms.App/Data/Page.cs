using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.FileSystemGlobbing.Internal;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text.RegularExpressions;

namespace Cms.Data
{
    public class Page
    {
        [Key]
        public int? id { get; set; }
        [Required]
        public string name { get; set; }
        public string? description { get; set; }
        [Required]
        [ParentPathValidation]
        public string path { get; set; }
        [ParentPathValidation]
        public int? parent { get; set; }
        public bool requiresAuth { get; set; }

        public virtual List<Site> Sites { get; set; } = new List<Site>();

        public Page(int _id, string name, string? description, string? path, int? parent, List<Site>? Sites)
        {
            this.id = _id;
            this.name = name;
            this.description = description;
            this.path = path;
            this.parent = parent;
            this.Sites = Sites;
        }
        public Page()
        {
        }
        public void CloneTo(Page page)
        {
            page.id = id;
            page.name = name;
            page.description = description;
            page.path = path;
            page.parent = parent;
        }
    }
    public class ParentPathValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var page = (Page)validationContext.ObjectInstance;
            if (page.parent == null && page.path == "/")
            {
                return ValidationResult.Success;
            }
            if (page.path!=null && Regex.IsMatch(page.path, @"^(/[A-Za-z]+)+$", RegexOptions.IgnoreCase))
            {
                return ValidationResult.Success;
            }
            return new ValidationResult("The 'path' field must start with a '/' character and contain at least one Latin letter.");
        }
    }
}