﻿namespace Cms.Data
{
    public class TreeNode<T>
    {
        public string Name { get; set; }
        public List<TreeNode<T>> Children { get; set; } = new List<TreeNode<T>>();
        public bool IsExpanded { get; set; }
        public T Object { get; set; }
        public bool isSelected { get; set; }
    }
}
