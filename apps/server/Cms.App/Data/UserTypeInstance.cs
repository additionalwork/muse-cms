using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cms.Data
{
    public class UserTypeInstance
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public List<Field> Fields { get; set; }

        public virtual List<Site> Sites { get; set; } = new List<Site>();

        public UserTypeInstance()
        {
            Fields = new List<Field>();
        }

        public UserTypeInstance(Guid Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
            Fields = new List<Field>();
        }

        public Dictionary<string, Field> GetFieldsDictionary()
        {
            Dictionary<string, Field> dic = new Dictionary<string, Field>();
            foreach (var item in Fields)
            {
                var f = new Field();
                item.CloneTo(f);
                dic.Add(f.Slug, f);
            }
            return dic;
        }

        public void CloneTo(UserTypeInstance instance)
        {
            instance.Id = Id;
            instance.Name = Name;
        }
        
        public static UserTypeInstance GetDefaultCopy(UserType Scheme)
        {
            var inst = new UserTypeInstance();
            inst.Name = "new element";
            for (int i = 0; i < Scheme.Fields.Count; i++)
            {
                var nf = new Field();
                Scheme.Fields[i].CloneTo(nf);
                nf.Value = Scheme.Fields[i].GetFieldDefaultValue();
                inst.Fields.Add(nf);
            }
            return inst;
        }
    }
}