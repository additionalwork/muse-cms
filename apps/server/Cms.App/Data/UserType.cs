using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cms.Data
{
    public class UserType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; } = "";

        public string? Description { get; set; }

        [Required]
        [RegularExpression(@"^(?!.*\b(integer|text|boolean|file)\b)[a-zA-Z\-_]*$", ErrorMessage = "Slug can contain only Latin letters and must not be one of the keywords")]
        public string Slug { get; set; } = "";

        [Required]
        public List<Field> Fields { get; set; } = new List<Field>();

        public virtual List<Site> Sites { get; set; } = new List<Site>();

        public bool Localized { get; set; } = false;

        public bool ProhibitNestedForm { get; set; } = false;
        
        public int Order { get; set; } = 0;

        public override bool Equals(object obj)
        {
            return obj is UserType other
                   && Fields.All(t => t.Equals(other.Fields.FirstOrDefault(f => f.Slug == t.Slug)))
                   && Id == other.Id
                   && Localized == other.Localized
                   && Slug == other.Slug
                   && Fields.Count == other.Fields.Count;
        }

        public void CloneTo(UserType userType)
        {
            userType.Id = Id;
            userType.Name = Name;
            userType.Description = Description;
            userType.Slug = Slug;
            userType.Localized = Localized;
            userType.ProhibitNestedForm = ProhibitNestedForm;
            userType.Order = Order;
            userType.Fields.RemoveAll(f => true);

            foreach (var f in Fields)
            {
                var nf = new Field();
                f.CloneTo(nf);
                userType.Fields.Add(nf);
            }
            userType.Fields = userType.Fields.OrderBy(f => f.Order).ToList();

            userType.Sites.RemoveAll(f => true);

            foreach (var site in Sites)
            {
                var newSite = new Site();
                site.CloneTo(newSite);
                userType.Sites.Add(newSite);
            }
        }

        public Dictionary<string, Field> GetFieldsDictionary()
        {
            Dictionary<string, Field> dic = new Dictionary<string, Field>();
            foreach (var item in Fields)
            {
                var f = new Field();
                item.CloneTo(f);
                if (!dic.ContainsKey(f.Slug))
                {
                    dic.Add(f.Slug, f);
                }
            }
            return dic;
        }

        [NotMapped]
        public static readonly string UserTablePrefix = "User_";

        [NotMapped]
        public static readonly string[] BaseTypes = new string[] { "text", "boolean", "integer", "file" };
    }
}