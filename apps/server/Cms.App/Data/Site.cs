using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cms.Data
{
    public class Site
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string? Description { get; set; }

        public List<string> Domains { get; set; } = new List<string>();

        public virtual List<UserType> UserTypes { get; set; } = new List<UserType>();

        public virtual List<UserTypeInstance> UserTypeInstances { get; set; } = new List<UserTypeInstance>();

        public virtual List<Page> Pages { get; set; } = new List<Page>();

        public virtual List<AppUser> AppUsers { get; set; } = new List<AppUser>();

        public void CloneTo(Site site)
        {
            site.Id = Id;
            site.Name = Name;
            site.Description = Description;
            site.Domains = Domains;
            site.UserTypes = UserTypes;
            site.UserTypeInstances = UserTypeInstances;
            site.Pages = Pages;
            site.AppUsers = AppUsers;
        }

        public void CloneFromContext(Site site, ApplicationDbContext dbContext)
        {
            var userIds = AppUsers.Select(user => user.Id).ToList();
            var pageIds = Pages.Select(page => page.id).ToList();
            site.Id = Id;
            site.Name = Name;
            site.Description = Description;
            site.Domains = Domains;
            AppUsers = new List<AppUser>();
            site.AppUsers = new List<AppUser>();
            Pages = new List<Page>();
            site.Pages = new List<Page>();
            site.UserTypes = new List<UserType>();
            UserTypes.ForEach((type) =>
            {
                var dbType = dbContext.UserTypes.FirstOrDefault((t) => t.Id == type.Id);
                if (dbType is not null)
                {
                    site.UserTypes.Add(dbType);
                }
            });
            site.UserTypeInstances = new List<UserTypeInstance>();
            UserTypeInstances.ForEach((instance) =>
            {
                var dbInstance = dbContext.UserTypeInstances.FirstOrDefault((inst) => inst.Id == instance.Id);
                if (dbInstance is not null)
                {
                    site.UserTypeInstances.Add(dbInstance);
                }
            });
            site.Pages = new List<Page>();
            pageIds.ForEach((id) =>
            {
                var dbPage = dbContext.Pages.FirstOrDefault(page => page.id == id);
                if (dbPage is not null)
                {
                    site.Pages.Add(dbPage);
                }
            });
            userIds.ForEach((id) =>
            {
                var dbUser = dbContext.Users.FirstOrDefault(user => user.Id == id);
                if (dbUser is not null)
                {
                    site.AppUsers.Add(dbUser);
                }
            });
            AppUsers = site.AppUsers;
            Pages = site.Pages;
        }
    }
}
