﻿using Microsoft.AspNetCore.Components.Forms;

namespace Cms.Services
{
    public class UploadFileService
    {
        public const string folderPath = "wwwroot/public/";

        public static async Task SaveFile(byte[] fileBytes, string newFilename, string oldFilename)
        {
            if (fileBytes == null || fileBytes.Length == 0)
                return;
            var newFilePath = Path.Combine(folderPath, newFilename);
            var oldFilePath = Path.Combine(folderPath, oldFilename);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            if (!String.IsNullOrEmpty(oldFilename) && File.Exists(oldFilePath))
            {
                File.Delete(oldFilePath);
            }
            using (var fileStream = new FileStream(newFilePath, FileMode.Create))
            {
                fileStream.Write(fileBytes, 0, fileBytes.Length); 
            }
            return;
        }

        public static async Task DeleteFile(string filename)
        {
            if (String.IsNullOrEmpty(filename))
                return;
            var oldFilePath = Path.Combine(folderPath, filename);
            if (File.Exists(oldFilePath))
            {
                File.Delete(oldFilePath);
            }
        }
    }
}
