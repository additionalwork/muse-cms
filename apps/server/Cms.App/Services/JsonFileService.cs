﻿using Cms.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;

namespace Cms.Services
{
    public class JsonFileService
    {
        private readonly ApplicationDbContext dbContext;
        private readonly string apiPrefix = "/api/v0";
        public JsonFileService(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        
        public string? GetJsonFile(ClaimsPrincipal? User, string? Domain = null)
        {
            var options = new JsonSerializerOptions { WriteIndented = true };
            Dictionary<int, Page> id_to_page = new Dictionary<int, Page>();
            List<Page> pages = dbContext.Pages.ToList();
            if (Domain is not null)
            {
                pages = dbContext.Sites
                    .FromSql($"SELECT * FROM \"Sites\" WHERE {Domain}=ANY(LOWER(\"Domains\"::text)::text[]) LIMIT 1")
                    .Include((site) => site.Pages)
                    .FirstOrDefault(
                        (site) => site != null
                    )?.Pages.ToList();
            }

            if (pages is null) return null;

            List<PageExport> exports = new List<PageExport>();

            foreach (var page in pages)
            {
                id_to_page.Add((int)page.id, page);
            }
            foreach (var page in pages)
            {
                List<string> bl = new List<string>() { page.path == "/" ? "" : page.path };
                Page parent;
                int? parentId = page.parent;
                while (parentId != null)
                {
                    parent = id_to_page[(int)parentId];
                    bl.Insert(0, parent.path == "/" ? "" : parent.path);
                    parentId = parent.parent;
                }
                exports.Add(new PageExport(page, apiPrefix + String.Join("", bl), GetNodes(page, User)));
            }
            string jsonString = JsonSerializer.Serialize(exports, options);

            return jsonString;
        }

        private List<BlockExport> GetNodes(Page selectedPage, ClaimsPrincipal? User)
        {
            List<BlockExport> RootNodes = new List<BlockExport>();
            List<Block> blocks = new List<Block>();
            if (selectedPage.requiresAuth == true && (User == null || !User.Identity.IsAuthenticated))
            {
                return RootNodes;
            }

            Block selectedBlock = new Block();

            blocks = dbContext.Blocks.Where(b => b.pageId == selectedPage.id).ToList();

            Dictionary<int, Block> id_to_block = new Dictionary<int, Block>();
            Dictionary<int, Block> id_to_prevBlock = new Dictionary<int, Block>();
            Dictionary<int, List<Block>> id_to_childs = new Dictionary<int, List<Block>>();
            foreach (var block in blocks)
            {
                id_to_block.Add((int)block.id, block);
                if (block.nextBlockId != null)
                    id_to_prevBlock.Add((int)block.nextBlockId, block);
                id_to_childs.Add((int)block.id, new List<Block>());
            }
            foreach (var block in blocks)
            {
                if (block.parent != null)
                    id_to_childs[(int)block.parent].Add(block);
            }

            Block? firstBlock = blocks.FirstOrDefault(b => b.parent == null);
            if (firstBlock == null)
                return RootNodes;
            while (id_to_prevBlock.ContainsKey((int)firstBlock.id))
            {
                firstBlock = id_to_prevBlock[(int)firstBlock.id];
            }
            while (true)
            {
                RootNodes.Add(new BlockExport(firstBlock, GetChilds(id_to_block, id_to_prevBlock, id_to_childs, (int)firstBlock.id)));
                if (firstBlock.nextBlockId == null)
                {
                    break;
                }
                firstBlock = id_to_block[(int)firstBlock.nextBlockId];
            }
            return RootNodes;
        }

        private List<BlockExport> GetChilds(Dictionary<int, Block> id_to_block, Dictionary<int, Block> id_to_prevBlock, Dictionary<int, List<Block>> id_to_childs, int parentId)
        {
            List<BlockExport> childs = new List<BlockExport>();
            Block? firstBlock = id_to_childs[parentId].FirstOrDefault();
            if (firstBlock == null)
                return childs;
            while (id_to_prevBlock.ContainsKey((int)firstBlock.id))
            {
                firstBlock = id_to_prevBlock[(int)firstBlock.id];
            }
            while (true)
            {
                childs.Add(new BlockExport(firstBlock, GetChilds(id_to_block, id_to_prevBlock, id_to_childs, (int)firstBlock.id)));
                if (firstBlock.nextBlockId == null)
                {
                    break;
                }
                firstBlock = id_to_block[(int)firstBlock.nextBlockId];
            }
            return childs;
        }
    }
}
