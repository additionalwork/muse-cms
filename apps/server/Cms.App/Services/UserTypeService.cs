using Cms.Data;
using Cms.Dto;
using Cms.Migrations;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text.Json;

namespace Cms.Services
{

    public class UserTypeService
    {
        private readonly ApplicationDbContext dbContext;

        public UserTypeService(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        // TODO: refactor!!!
        public List<UserTypeResultDto>? GetUserTypes(string? domain = null)
        {
            List<UserType>? userTypes = domain != null ? dbContext.GetSiteUserTypes(domain) : dbContext.GetUserTypes();
            if (userTypes is null) return null;
            var site = domain == null ? null : dbContext.Sites.FirstOrDefault(s => s.Domains.Contains(domain));
            var dtos = userTypes
                .Select(u => new UserTypeResultDto(u, dbContext.GetUserTypeInstancesRows(u, site))).ToList();
            return dtos;
        }

        public UserType? GetUserType(string slug, string? domain = null)
        {
            IQueryable<UserType> userTypesQuery;
            if (domain is not null)
            {
                var site = dbContext.Sites
                    .FromSql($"SELECT * FROM \"Sites\" WHERE {domain}=ANY(LOWER(\"Domains\"::text)::text[]) LIMIT 1")
                    .Include((site) => site.UserTypes)
                    .FirstOrDefault((site) => site != null);
                if (site is null) return null;
                userTypesQuery = site.UserTypes.AsQueryable();
            } else {
                userTypesQuery = dbContext.UserTypes;
            }
            UserType? userType = userTypesQuery.Include(u => u.Fields).FirstOrDefault(userType => userType.Slug == slug);
            return userType;
        }

        private Dictionary<string, Dictionary<string, Dictionary<string, object>>> AddTypesFromFields(string slug, Dictionary<string, Dictionary<string, Dictionary<string, object>>> dict, string? domain = null, string lang = CustomTable.DefaultLanguage)
        {
            var userType = dbContext.FindUserTypeBySlug(slug);
            var site = domain==null?null:dbContext.Sites.Include((site) => site.UserTypeInstances).FirstOrDefault(s => s.Domains.Contains(domain));
            dict.Add(userType.Slug, dbContext.getUserTypeInstancesRowsDic(userType, site, lang));
            
            foreach (var field in userType.Fields.Where(field => !UserType.BaseTypes.Contains(field.Type) && !dict.ContainsKey(field.Type)))
            {
                AddTypesFromFields(field.Type, dict, domain,lang);
            }

            return dict;
        }

        public Dictionary<string, Dictionary<string, object>> GetUserTypeNested(string targetTableSlug, string? domain = null, string lang = CustomTable.DefaultLanguage)
        {
            
            var userTypesDic = dbContext.GetUserTypes().ToDictionary(x => x.Slug);
            var userTypeSlugToInst = AddTypesFromFields(targetTableSlug, new Dictionary<string, Dictionary<string, Dictionary<string, object>>>(), domain,lang);

            foreach (var tableSlug in userTypeSlugToInst.Keys)
            {
                var curUserType = userTypesDic[tableSlug];
                foreach (var field in curUserType.Fields)
                {
                    if (field.IsArray)
                    {
                        foreach (var insts in userTypeSlugToInst[tableSlug].Values)
                        {
                            if (insts[field.Slug] == null || insts[field.Slug] == "")
                            {
                                insts[field.Slug] = new List<string>();
                                continue;
                            }
                            var list = JsonSerializer.Deserialize<List<string>>(insts[field.Slug].ToString());
                            if (field.Type == "file")
                            {
                                for (int i = 0; i < list.Count; i++)
                                {
                                    if (!string.IsNullOrEmpty(list[i]) && !list[i].StartsWith("/public"))
                                        list[i] = "/public/" + list[i];
                                }
                            }
                            insts[field.Slug] = list;
                        }
                    }
                    else if (field.Type == "file")
                    {
                        foreach (var insts in userTypeSlugToInst[tableSlug].Values)
                        {
                            if (insts[field.Slug] == null) continue;

                            var str = insts[field.Slug].ToString();
                            if (!string.IsNullOrEmpty(str) && !str.StartsWith("/public"))
                                insts[field.Slug] = "/public/" + str;
                        }
                    }
                }
            }
            foreach (var tableSlug in userTypeSlugToInst.Keys)
            {
                var curUserType = userTypesDic[tableSlug];
                foreach (var field in curUserType.Fields)
                {
                    if (UserType.BaseTypes.Contains(field.Type))
                    { continue; }
                    foreach (var insts in userTypeSlugToInst[tableSlug].Values)
                    {
                        if (insts[field.Slug] == null || insts[field.Slug] == "")
                        { continue; }

                        //link field slug must be equal to table slug
                        var toReplaceList = userTypeSlugToInst[field.Type];

                        if (field.IsArray)
                        {
                            var listIds = (List<string>)insts[field.Slug];
                            var newValue = listIds.Select(id => toReplaceList[id]).ToList();
                            insts[field.Slug] = newValue;
                        }
                        else
                        {
                            var id = insts[field.Slug].ToString();
                            insts[field.Slug] = toReplaceList[id];
                        }
                    }
                }
            }
            return userTypeSlugToInst[targetTableSlug];
        }
    }
}
