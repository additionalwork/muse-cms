using Cms.Data;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Cms.Services
{
    public class SiteService
    {
        private ApplicationDbContext _dbContext;
        private readonly AuthenticationStateProvider _authenticationStateProvider;
        public Site SelectedSite { get; set; } = new();
        
        public SiteService(
            ApplicationDbContext dbContext,
            AuthenticationStateProvider authenticationStateProvider
        )
        {
            _dbContext = dbContext;
            _authenticationStateProvider = authenticationStateProvider;
        }
        
        public async Task<List<Site>> GetSiteList()
        {
            _dbContext = _dbContext.ReCreate();
            var authState = await _authenticationStateProvider.GetAuthenticationStateAsync();
            var user = authState.User;

            IQueryable<Site> siteQuery = _dbContext.Sites;
            if (!user.IsInRole("Administrator"))
            {
                siteQuery = siteQuery
                    .Where((site) => site.AppUsers.Any((appUser) => appUser.UserName == user.Identity.Name));
            }

            var sites = siteQuery
                .Include((site) => site.UserTypes)
                .Include((site) => site.AppUsers)
                .Include((site) => site.Pages)
                .ToList();
            return sites;
        }
        
        public async Task<List<Site>> GetSiteListWithAllFields()
        {
            _dbContext = _dbContext.ReCreate();
            var authState = await _authenticationStateProvider.GetAuthenticationStateAsync();
            var user = authState.User;

            IQueryable<Site> siteQuery = _dbContext.Sites;
            if (!user.IsInRole("Administrator"))
            {
                siteQuery = siteQuery
                    .Where((site) => site.AppUsers.Any((appUser) => appUser.UserName == user.Identity.Name));
            }

            var sites = siteQuery
                .Include((site) => site.Pages)
                .Include((site) => site.UserTypes)
                    .ThenInclude((userType) => userType.Fields)
                .Include((site) => site.UserTypeInstances)
                .Include((site) => site.AppUsers)
                .ToList();
            
            return sites;
        }
        
        public async Task<List<UserTypeInstance>> GetSiteWithUserTypeInstances(Guid id)
        {
            _dbContext = _dbContext.ReCreate();
            var authState = await _authenticationStateProvider.GetAuthenticationStateAsync();
            var user = authState.User;

            IQueryable<Site> siteQuery = _dbContext.Sites;
            if (!user.IsInRole("Administrator"))
            {
                siteQuery = siteQuery
                    .Where((site) => site.AppUsers.Any((appUser) => appUser.UserName == user.Identity.Name));
            }

            var site = siteQuery
                .Include((site) => site.UserTypeInstances)
                .First(el => el.Id == id);
            
            return site.UserTypeInstances;
        }

        public async Task<List<Site>> GetSiteList(ApplicationDbContext context)
        {
            var authState = await _authenticationStateProvider.GetAuthenticationStateAsync();
            var user = authState.User;

            IQueryable<Site> siteQuery = context.Sites;
            if (!user.IsInRole("Administrator"))
            {
                siteQuery = siteQuery
                    .Where((site) => site.AppUsers.Any((appUser) => appUser.UserName == user.Identity.Name));
            }

            var sites = siteQuery
                .Include((site) => site.UserTypes)
                .Include((site) => site.AppUsers)
                .Include((site) => site.Pages)
                .ToList();
            return sites;
        }
    }
}
