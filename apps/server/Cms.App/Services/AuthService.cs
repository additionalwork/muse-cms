﻿using Cms.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Cms.Services
{
    public class AuthResult
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public bool Success { get; set; }
    }
    public class AuthService
    {
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;

        public AuthService(SignInManager<AppUser> signInManager, UserManager<AppUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<AuthResult> Login(string username, string password)
        {
            var result = await _signInManager.PasswordSignInAsync(username, password, false, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                var user = await _signInManager.UserManager.FindByNameAsync(username);
                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    // Add any additional claims as needed
                };
                var token = await GenerateToken(claims);
                var refreshToken = await GenerateRefreshToken(user);

                return new AuthResult
                {
                    AccessToken = token,
                    RefreshToken = refreshToken,
                    Success = true
                };
            }
            return new AuthResult
            {
                Success = false
            };
        }

        public async Task<AuthResult> RefreshToken(string username, string refreshToken)
        {
            AppUser? user = await _signInManager.UserManager.FindByNameAsync(username);
            if (user == null)
            {
                return new AuthResult
                {
                    Success = false
                };
            }
            //var refreshToken = await _userManager.GetAuthenticationTokenAsync(user, "MyApp", "RefreshToken");
            var isValid = await _userManager.GetAuthenticationTokenAsync(user, "MyApp", "RefreshToken") == refreshToken;
            var isAlive = IsRefreshTokenValid(refreshToken);

            if (isValid && isAlive)
            {
                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    // Add any additional claims as needed
                };
                var token = await GenerateToken(claims);
                var newrefreshToken = await GenerateRefreshToken(user);

                return new AuthResult
                {
                    AccessToken = token,
                    RefreshToken = newrefreshToken,
                    Success = true
                };
            }
            return new AuthResult
            {
                Success = false
            };
        }

        private async Task<string> GenerateRefreshToken(AppUser user)
        {
            AuthOptions opt = new AuthOptions();
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(opt.SecretKeyRefresh));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTime.UtcNow.AddDays(opt.ExpirationDays),
                Issuer = opt.Issuer,
                Audience = opt.Audience,
                SigningCredentials = signingCredentials
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            var newRefreshToken = tokenHandler.WriteToken(token);

            await _userManager.RemoveAuthenticationTokenAsync(user, "MyApp", "RefreshToken");
            await _userManager.SetAuthenticationTokenAsync(user, "MyApp", "RefreshToken", newRefreshToken);
            return newRefreshToken;
        }

        private async Task<string> GenerateToken(Claim[] claims)
        {
            var opt = new AuthOptions();
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(opt.SecretKey));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddMinutes(Convert.ToDouble(opt.ExpirationMins)),
                Issuer = opt.Issuer,
                Audience = opt.Audience,
                SigningCredentials = signingCredentials
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
        private bool IsRefreshTokenValid(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenValidationParameters = new AuthOptions().MyRefreshTokenValidationParameters();

            try
            {
                // Пытаемся валидировать токен с помощью JwtSecurityTokenHandler
                var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out var validatedToken);
                return true; // Если токен валиден, возвращаем true
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                // Если возникла ошибка валидации токена, значит, он протух
                return false;
            }
        }

    }
}
