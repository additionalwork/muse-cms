﻿using Cms.Data;
using System.ComponentModel.DataAnnotations;

namespace Cms.Dto
{
    public record UserTypeResultDto
    {
        public UserTypeResultDto(UserType userType, List<Dictionary<string, object>> instaces) {
            Id = userType.Id;
            Scheme = userType;
            Scheme.Sites = new List<Site>();
            InstancesRows = instaces;
        }

        [Required]
        public Guid Id { get; set; }
        [Required]
        public UserType Scheme { get; set; }
        public List<Dictionary<string, object>> InstancesRows { get; set; }
    }
}
