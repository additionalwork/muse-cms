﻿using Microsoft.AspNetCore.Mvc;

namespace Cms.Dto
{
    public record LoginResultDto
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }

        public LoginResultDto(string accessToken, string refreshToken)
        {
            RefreshToken = refreshToken;
            AccessToken = accessToken;
        }
    }
}
