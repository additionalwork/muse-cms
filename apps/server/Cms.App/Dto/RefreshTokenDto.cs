﻿namespace Cms.Dto
{
    public record RefreshTokenDto
    {
        public string Username { get; set; }
        public string RefreshToken { get; set; }
    }
}
