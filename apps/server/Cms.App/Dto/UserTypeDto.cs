﻿namespace Cms.Dto
{
    public record UserTypeDTO
    {
        public UserTypeDTO(List<Dictionary<string, object>> instaces) {
            Profiles = instaces;
        }

        public List<Dictionary<string, object>> Profiles { get; set; }
    }
}
