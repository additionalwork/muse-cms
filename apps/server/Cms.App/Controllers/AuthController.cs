﻿using Cms.Dto;
using Cms.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Cms.Controllers
{
    [ApiController]
    [Route("api/v0/Auth")]
    public class AuthController : ControllerBase
    {
        private readonly AuthService _authService;

        public AuthController(AuthService authService)
        {
            _authService = authService;
        }

        [HttpPost]
        public async Task<ActionResult<LoginResultDto>> Login([FromBody] LoginDto loginDto)
        {
            string username = loginDto.Email;
            string password = loginDto.Password;

            var authResult = await _authService.Login(username, password);
            if (authResult.Success)
            {
                return Ok(new LoginResultDto(authResult.AccessToken, authResult.RefreshToken));
            }

            return Unauthorized();
        }

        [HttpPost("refresh")]
        public async Task<ActionResult<LoginResultDto>> RefreshToken([FromBody] RefreshTokenDto refreshTokenDto)
        {
            string username = refreshTokenDto.Username;
            string refreshToken = refreshTokenDto.RefreshToken;
            var authResult = await _authService.RefreshToken(username, refreshToken);
            if (authResult.Success)
            {
                return Ok(new LoginResultDto(authResult.AccessToken, authResult.RefreshToken));
            }

            return Unauthorized();
        }
    }
}
