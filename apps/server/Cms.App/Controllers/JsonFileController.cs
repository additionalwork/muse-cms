﻿using Cms.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Cms.Controllers
{
    [ApiController]
    [Route("api/v0/JsonFile")]
    public class JsonFileController : ControllerBase
    {
        private readonly JsonFileService jsonFileService;

        public JsonFileController(JsonFileService jsonFileService)
        {
            this.jsonFileService = jsonFileService;
        }
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("auth")]
        public ActionResult<string> GetJsonFile()
        {
            string jsonString = jsonFileService.GetJsonFile(User);
            return Ok(jsonString);
        }
        [HttpGet]
        public ActionResult<string> GetJsonFileNotAuth()
        {  
            string? domain = HttpContext.Request.Headers.Host;
            string? jsonString = jsonFileService.GetJsonFile(null, domain);
            if (jsonString is null) return NotFound();
            return Ok(jsonString);
        }
    }
}
