﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json;
using Cms.Data;
using Cms.Dto;
using Cms.Migrations;
using Cms.Services;
using Microsoft.AspNetCore.Mvc;

namespace Cms.Controllers
{
    [ApiController]
    [Route("api/v0/usertype")]
    public class UserTypeController : ControllerBase
    {
        private readonly UserTypeService _userTypeService;

        private readonly ApplicationDbContext _dbContext;
        
        public UserTypeController(UserTypeService userTypeService, ApplicationDbContext dbContext)
        {
            _userTypeService = userTypeService;
            _dbContext = dbContext;
        }


        // TODO: Revise the UserTypeResultDto entity. Inside is UserType
        [HttpGet]
        public ActionResult<List<UserTypeResultDto>> GetUserTypes()
        {
            string? domain = HttpContext.Request.Headers.Host;
            List<UserTypeResultDto>? dto = _userTypeService.GetUserTypes(domain);
            if (dto is null) return NotFound();
            return Ok(dto);
        }

        [HttpGet]
        [Route("{slug}")]
        public ActionResult<UserTypeDTO> GetUserType([FromRoute][Required] string slug)
        {
            string? domain = HttpContext.Request.Headers.Host;
            string? lang = HttpContext.Request.Headers.AcceptLanguage;
            UserType? userType = _userTypeService.GetUserType(slug, domain);
            if (userType == null)
            {
                return NotFound();
            }
            if (!CustomTable.LanguageList.Contains(lang))
            {
                lang = CustomTable.DefaultLanguage;
            }

            var dic = _userTypeService.GetUserTypeNested(slug, domain, lang);
            var userTypeDto = new UserTypeDTO(dic.Values.ToList());
            return Ok(userTypeDto);
        }

        [HttpGet]
        [Route("{slug}/{id}")]
        public ActionResult<UserTypeDTO> GetUserTypeById([FromRoute][Required] string slug, [FromRoute][Required] string id)
        {
            string? domain = HttpContext.Request.Headers.Host;
            string? lang = HttpContext.Request.Headers.AcceptLanguage;
            UserType? userType = _userTypeService.GetUserType(slug, domain);
            if (userType == null)
            {
                return NotFound();
            }
            if (!CustomTable.LanguageList.Contains(lang))
            {
                lang = CustomTable.DefaultLanguage;
            }

            var dic = _userTypeService.GetUserTypeNested(slug, domain, lang);
            var instance = dic.Where(u => u.Key == id).FirstOrDefault();
            if (instance.Key == null)
            {
                return NotFound();
            }
            return Ok(instance);
        }
    }
}
