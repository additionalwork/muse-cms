﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Cms.Migrations
{
    /// <inheritdoc />
    public partial class DefaultUsersAndRoles : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //user
            migrationBuilder.InsertData(
               "AspNetUsers", new string[] { "Id","FirstName", "LastName", "UserName", "NormalizedUserName", "Email", "NormalizedEmail", "EmailConfirmed", "PasswordHash", "SecurityStamp", "ConcurrencyStamp", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEnd", "LockoutEnabled", "AccessFailedCount" },
               new object[] { "cec6f7c7-a38e-4bed-98bd-0ade483de637", "Илья", "Талант", "admin@example.com", "ADMIN@EXAMPLE.COM", "admin@example.com", "ADMIN@EXAMPLE.COM", true, "AQAAAAIAAYagAAAAEAXxApqdiDEEtRA/+NkUqkqqfU8evkisAGJplLDqdXtGlNREmMatzNQJw0NKwsumCQ==", "GJXUJI2EQ6DWL5RYPELCOANQYFYMQ5FA", "d308acbb-978a-4fd8-8bb8-d43885ce66fb", null, false, false, null, true, 0 });


            //roles
            migrationBuilder.InsertData(
                "AspNetRoles", new string[] { "Id", "Name", "NormalizedName", "ConcurrencyStamp" },
                new object[] { "f02e1f85-88c1-422d-9039-1fc80fc8f013", "Administrator", "ADMINISTRATOR", null });
            migrationBuilder.InsertData(
               "AspNetRoles", new string[] { "Id", "Name", "NormalizedName", "ConcurrencyStamp" },
               new object[] { "06de3d06-6d3a-4579-ad20-721488dc2e57", "Moderator", "MODERATOR", null });
            migrationBuilder.InsertData(
               "AspNetRoles", new string[] { "Id", "Name", "NormalizedName", "ConcurrencyStamp" },
               new object[] { "366e18ba-ceb1-4178-bd31-77d306b2c415", "User", "USER", null });

            migrationBuilder.InsertData(
               "AspNetUserRoles", new string[] { "UserId", "RoleId" },
               new object[] { "cec6f7c7-a38e-4bed-98bd-0ade483de637", "f02e1f85-88c1-422d-9039-1fc80fc8f013" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
               name: "AspNetUserRoles");

            migrationBuilder.DropTable(
              name: "AspNetUsers");
        }
    }
}
