﻿using Microsoft.EntityFrameworkCore.Migrations;
using Cms.Data;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

#nullable disable

namespace Cms.Migrations
{
    /// <inheritdoc />
    public partial class CustomTable : Migration
    {
        public UserType table;
        public UserType oldTable;
        public List<Field> toRemove = new List<Field>();
        public Dictionary<Field, Field> toUpdate = new Dictionary<Field, Field>();
        public List<Field> toAdd = new List<Field>();
        public string TableName = "";
        public string LocalizedTableName = "";
        public static readonly List<string> LanguageList = new() { "Ru", "En", "Fr", "De", "Zh" };
        public const string DefaultLanguage = "Ru";
        
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            if (table == null) return;
            
            TableName = UserType.UserTablePrefix + table.Slug;
            LocalizedTableName = TableName + "_Ru";
            
            if (table.Id != Guid.Empty)
            {
                AlterTable(migrationBuilder);
                return;
            }
            
            migrationBuilder.CreateTable(
                name: TableName,
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_" + TableName + "_Id", x => x.Id);
                });
            migrationBuilder.AlterTable(TableName, null, table.Name + '\n' + table.Description);
            
            if (!table.Localized)
            {
                table.Fields.ForEach(field =>
                {
                    migrationBuilder.AddColumn<string>(
                        name: field.Slug,
                        table: TableName,
                        type: field.GetDatabaseType(),
                        nullable: field.IsNullable,
                        defaultValue: field.GetFieldDefaultValue());
                });
            }
            else
            {
                var defaultFields = table.Fields.FindAll(field => !field.Localized);
                var localizedFields = table.Fields.FindAll(field => field.Localized);

                CreateLocalizedTable(migrationBuilder);
                
                defaultFields.ForEach(field =>
                {
                    migrationBuilder.AddColumn<string>(
                        name: field.Slug,
                        table: TableName,
                        type: field.GetDatabaseType(),
                        nullable: field.IsNullable,
                        defaultValue: field.GetFieldDefaultValue());
                });
                
                localizedFields.ForEach(field =>
                {
                    LanguageList.ForEach(el => 
                        migrationBuilder.AddColumn<string>(
                            name: field.Slug,
                            table: TableName + "_" + el,
                            type: field.GetDatabaseType(),
                            nullable: true,
                            defaultValue: field.GetFieldDefaultValue())
                    );
                });
            }
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            if (table == null) return;
            string TableName = UserType.UserTablePrefix + table.Slug;
            if (table.Localized)
            {
                LanguageList.ForEach(el => 
                        migrationBuilder.DropTable(
                            name: TableName + "_" + el)
                    );
            }
            migrationBuilder.DropTable(
                name: TableName);
        }
    
        private void CreateLocalizedTable(MigrationBuilder migrationBuilder)
        {
            LanguageList.ForEach(el =>
            {
                migrationBuilder.CreateTable(
                    name: TableName + "_" + el,
                    columns: table => new
                    {
                        Id = table.Column<Guid>(type: "uuid", nullable: false),
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_" + TableName + el, x => x.Id);
                        table.ForeignKey(
                            name: "FK" + TableName + el + "_UserTypeId",
                            column: x => x.Id,
                            principalTable: TableName,
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade);
                    }
                );
            });
        }

        private void AlterTable(MigrationBuilder migrationBuilder)
        {
            if (oldTable.Slug != table.Slug)
            {
                string oldTableName = UserType.UserTablePrefix + oldTable.Slug;
                migrationBuilder.RenameTable(oldTableName, null, TableName, null);
                if (oldTable.Localized)
                {
                    LanguageList.ForEach(el => migrationBuilder.RenameTable(oldTableName + "_" + el, null, TableName + "_" + el, null));
                }
            }

            if (oldTable.Localized != table.Localized)
            {
                if (!oldTable.Localized)
                {
                    CreateLocalizedTable(migrationBuilder);
                    LanguageList.ForEach(el => migrationBuilder.Sql($"INSERT INTO \"{TableName + "_" + el}\" (\"Id\") SELECT \"Id\" FROM \"{TableName}\";"));
                }
            }

            foreach (var row in toUpdate)
            {
                Field oldField = row.Key;
                Field newField = row.Value;

                if (oldField.GetDatabaseType() != newField.GetDatabaseType())
                {
                    toRemove.Add(oldField);
                    toAdd.Add(newField);
                    continue;
                }

                if (oldField.Slug != newField.Slug)
                {
                    if (oldField.Localized)
                    {
                        LanguageList.ForEach(el =>
                        {
                            migrationBuilder.RenameColumn(
                                name: oldField.Slug,
                                table: TableName + "_" + el,
                                newName: newField.Slug);
                        });
                    }
                    else
                    {
                        migrationBuilder.RenameColumn(
                            name: oldField.Slug,
                            table: TableName,
                            newName: newField.Slug);
                    }
                }

                if (oldField.Localized != newField.Localized)
                {
                    var oldTableName = oldField.Localized ? LocalizedTableName : TableName;

                    if (oldField.Localized)
                    {
                        migrationBuilder.AddColumn<string>(
                            name: newField.Slug,
                            table: TableName,
                            type: newField.GetDatabaseType(),
                            nullable: newField.IsNullable,  
                            defaultValue: newField.GetFieldDefaultValue());
                        
                        migrationBuilder
                            .Sql($"UPDATE \"{TableName}\" SET \"{newField.Slug}\" = " 
                                 + $"(SELECT \"{oldField.Slug}\" FROM \"{oldTableName}\" WHERE \"{oldTableName}\".\"Id\" = \"{TableName}\".\"Id\");");
                        
                        LanguageList.ForEach(el =>
                            migrationBuilder.DropColumn(
                                name: newField.Slug,
                                table: TableName + "_" + el)
                        );
                    }
                    else
                    {
                        LanguageList.ForEach(el =>
                            migrationBuilder.AddColumn<string>(
                                name: newField.Slug,
                                table: TableName + "_" + el,
                                type: newField.GetDatabaseType(),
                                nullable: true,
                                defaultValue: newField.GetFieldDefaultValue())
                        );
                        
                        migrationBuilder
                            .Sql($"UPDATE \"{TableName + "_Ru"}\" SET \"{newField.Slug}\" = "
                                 + $"(SELECT \"{oldField.Slug}\" FROM \"{oldTableName}\" WHERE \"{oldTableName}\".\"Id\" = \"{TableName + "_Ru"}\".\"Id\");");
                    
                        migrationBuilder.DropColumn(
                            name: newField.Slug,
                            table: TableName);
                    }
                }

                if (newField.Localized)
                {
                    LanguageList.ForEach(el => 
                        migrationBuilder.AlterColumn<string>(
                            name: newField.Slug,
                            table: TableName + "_" + el,
                            type: newField.GetDatabaseType(),
                            nullable: true)    
                    );
                }
                else
                {
                    migrationBuilder.AlterColumn<string>(
                            name: newField.Slug,
                            table: TableName,
                            type: newField.GetDatabaseType(),
                            nullable: newField.IsNullable);
                }
                
            }

            foreach (var field in toRemove)
            {
                if (field.Localized)
                {
                   LanguageList.ForEach(el =>
                       migrationBuilder.DropColumn(
                           name: field.Slug,
                           table: TableName + "_" + el));
                }
                else
                {
                    migrationBuilder.DropColumn(
                        name: field.Slug,
                        table: TableName);
                }
            }

            foreach (var field in toAdd)
            {
                if (field.Localized)
                {
                    LanguageList.ForEach(el =>
                        migrationBuilder.AddColumn<string>(
                            name: field.Slug,
                            table: TableName + "_" + el,
                            type: field.GetDatabaseType(),
                            nullable: true,
                            defaultValue: field.GetFieldDefaultValue()));
                }
                else
                {
                    migrationBuilder.AddColumn<string>(
                        name: field.Slug,
                        table: TableName,
                        type: field.GetDatabaseType(),
                        nullable: field.IsNullable,
                        defaultValue: field.GetFieldDefaultValue());
                }
            }
            
            if (oldTable.Localized != table.Localized)
            {
                if (oldTable.Localized)
                {
                    LanguageList.ForEach(el => migrationBuilder.DropTable(name: TableName + "_" + el));
                }
            }
        }
    }
}
