namespace Cms.Data;

public interface IMetamodelAccessor
{
   Metamodel Metamodel { get; }
}
