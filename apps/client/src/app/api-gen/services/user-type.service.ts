/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';

import { UserTypeDto } from '../models/user-type-dto';
import { UserTypeResult } from '../models/user-type-result';

@Injectable({ providedIn: 'root' })
export class UserTypeService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `getUserTypes()` */
  static readonly GetUserTypesPath = '/api/v0/usertype';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUserTypes$Plain()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserTypes$Plain$Response(params?: {
  }
): Observable<StrictHttpResponse<Array<UserTypeResult>>> {

    const rb = new RequestBuilder(this.rootUrl, UserTypeService.GetUserTypesPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: 'text/plain'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<UserTypeResult>>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUserTypes$Plain$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserTypes$Plain(params?: {
  }
): Observable<Array<UserTypeResult>> {

    return this.getUserTypes$Plain$Response(params).pipe(
      map((r: StrictHttpResponse<Array<UserTypeResult>>) => r.body as Array<UserTypeResult>)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUserTypes()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserTypes$Response(params?: {
  }
): Observable<StrictHttpResponse<Array<UserTypeResult>>> {

    const rb = new RequestBuilder(this.rootUrl, UserTypeService.GetUserTypesPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'text/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<UserTypeResult>>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUserTypes$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserTypes(params?: {
  }
): Observable<Array<UserTypeResult>> {

    return this.getUserTypes$Response(params).pipe(
      map((r: StrictHttpResponse<Array<UserTypeResult>>) => r.body as Array<UserTypeResult>)
    );
  }

  /** Path part for operation `getUserType()` */
  static readonly GetUserTypePath = '/api/v0/usertype/{slug}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUserType$Plain()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserType$Plain$Response(params: {
    slug: string;
  }
): Observable<StrictHttpResponse<UserTypeDto>> {

    const rb = new RequestBuilder(this.rootUrl, UserTypeService.GetUserTypePath, 'get');
    if (params) {
      rb.path('slug', params.slug, {});
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: 'text/plain'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UserTypeDto>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUserType$Plain$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserType$Plain(params: {
    slug: string;
  }
): Observable<UserTypeDto> {

    return this.getUserType$Plain$Response(params).pipe(
      map((r: StrictHttpResponse<UserTypeDto>) => r.body as UserTypeDto)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUserType()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserType$Response(params: {
    slug: string;
  }
): Observable<StrictHttpResponse<UserTypeDto>> {

    const rb = new RequestBuilder(this.rootUrl, UserTypeService.GetUserTypePath, 'get');
    if (params) {
      rb.path('slug', params.slug, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'text/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UserTypeDto>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getUserType$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserType(params: {
    slug: string;
  }
): Observable<UserTypeDto> {

    return this.getUserType$Response(params).pipe(
      map((r: StrictHttpResponse<UserTypeDto>) => r.body as UserTypeDto)
    );
  }

}
