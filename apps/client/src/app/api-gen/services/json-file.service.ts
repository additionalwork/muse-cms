/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';


@Injectable({ providedIn: 'root' })
export class JsonFileService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `getJsonFile()` */
  static readonly GetJsonFilePath = '/api/v0/JsonFile/auth';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getJsonFile$Plain()` instead.
   *
   * This method doesn't expect any request body.
   */
  getJsonFile$Plain$Response(params?: {
  }
): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, JsonFileService.GetJsonFilePath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: 'text/plain'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getJsonFile$Plain$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getJsonFile$Plain(params?: {
  }
): Observable<string> {

    return this.getJsonFile$Plain$Response(params).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getJsonFile()` instead.
   *
   * This method doesn't expect any request body.
   */
  getJsonFile$Response(params?: {
  }
): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, JsonFileService.GetJsonFilePath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'text/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getJsonFile$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getJsonFile(params?: {
  }
): Observable<string> {

    return this.getJsonFile$Response(params).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

  /** Path part for operation `getJsonFileNotAuth()` */
  static readonly GetJsonFileNotAuthPath = '/api/v0/JsonFile';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getJsonFileNotAuth$Plain()` instead.
   *
   * This method doesn't expect any request body.
   */
  getJsonFileNotAuth$Plain$Response(params?: {
  }
): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, JsonFileService.GetJsonFileNotAuthPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'text',
      accept: 'text/plain'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getJsonFileNotAuth$Plain$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getJsonFileNotAuth$Plain(params?: {
  }
): Observable<string> {

    return this.getJsonFileNotAuth$Plain$Response(params).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getJsonFileNotAuth()` instead.
   *
   * This method doesn't expect any request body.
   */
  getJsonFileNotAuth$Response(params?: {
  }
): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, JsonFileService.GetJsonFileNotAuthPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'text/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getJsonFileNotAuth$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getJsonFileNotAuth(params?: {
  }
): Observable<string> {

    return this.getJsonFileNotAuth$Response(params).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

}
