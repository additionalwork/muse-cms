/* tslint:disable */
/* eslint-disable */
import { Field } from './field';
export interface UserType {
  description?: null | string;
  fields: Array<Field>;
  id: string;
  name: string;
  slug: string;
}
