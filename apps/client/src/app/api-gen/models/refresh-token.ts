/* tslint:disable */
/* eslint-disable */
export interface RefreshToken {
  refreshToken?: null | string;
  username?: null | string;
}
