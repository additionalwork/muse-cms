/* tslint:disable */
/* eslint-disable */
export interface LoginResult {
  accessToken?: null | string;
  refreshToken?: null | string;
}
