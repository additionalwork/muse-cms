/* tslint:disable */
/* eslint-disable */
export interface Field {
  description?: null | string;
  id: string;
  isNullable?: boolean;
  name: string;
  slug: string;
  type: string;
  value?: null | any;
}
