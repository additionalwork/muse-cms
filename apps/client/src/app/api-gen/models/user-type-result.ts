/* tslint:disable */
/* eslint-disable */
import { UserType } from './user-type';
export interface UserTypeResult {
  id: string;
  instancesRows?: null | Array<{
[key: string]: any;
}>;
  scheme: UserType;
}
