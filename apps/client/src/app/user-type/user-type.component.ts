import { Component, Input, OnInit } from '@angular/core';
import { UserTypeStore } from '../store/usertype.repository';
import { UserType } from '../api-gen/models';
import { Instance, InstanceStore } from '../store/instance.repository';

@Component({
  selector: 'app-user-type',
  templateUrl: './user-type.component.html',
  styleUrls: ['./user-type.component.scss'],
})
export class UserTypeComponent implements OnInit {
  @Input() tableSlug: string | undefined;
  private _recordId: string[] | undefined = undefined;
  @Input()
  get recordId(): string | undefined {
    return this._recordId?.join(',');
  }
  set recordId(val: string | undefined) {
    if(val == undefined) return;
    if (!val?.includes(',')) this._recordId = [val];
    else this._recordId = val.split(',');
  }

  usertype: UserType | undefined;
  instances: Instance[]=[];
  error = '';
  showAll = false;
  baseTypes = ["text", "integer", "boolean", "list", "file"]

  constructor(private usertypeStore: UserTypeStore) {}

  ngOnInit() {
    setTimeout(() =>
      console.log('UserTypeComponent ', this.usertype, this.error)
    );
    if (!this.tableSlug) {
      this.error = 'Table not selected';
      return;
    }
    this.usertype = this.usertypeStore.getUserTypesBySlug(this.tableSlug);
    if (!this.usertype) {
      this.error = 'There is no such table '+this.tableSlug;
      return;
    }
    const instStore = new InstanceStore(this.tableSlug);
    this.instances = instStore.getInstances();
    console.log(" this.instances ",this.tableSlug, this.instances)
    console.log(" _recordId ", this._recordId)
  }
}
