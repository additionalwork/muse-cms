import { TestBed, ComponentFixture } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppComponent } from './app.component';
import { MyBuilderComponent, Page } from 'cms-extensions';

 describe('AppComponent', () => {
   let component: AppComponent;
   let fixture: ComponentFixture<AppComponent>;
   let httpMock: HttpTestingController;

   beforeEach(async () => { // Remove the `async(() => ...)` wrapper
    await TestBed.configureTestingModule({ // Add `await` before `TestBed.configureTestingModule`
      declarations: [AppComponent, MyBuilderComponent],
      imports: [HttpClientTestingModule]
    }).compileComponents();
  
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    httpMock = TestBed.inject(HttpTestingController);
  });
  

   it('should create the app', () => {
     expect(component).toBeTruthy();
   });

   it('should fetch data from /assets/data.json', () => {
     const testData = { data: ['test1', 'test2', 'test3'] };
     component.serverURL = '/assets/data.json';
     component.ngOnInit();
     const req = httpMock.expectOne('/assets/data.json');
     req.flush(testData);

     expect(component.pages).toEqual(testData as unknown as Page[]);
     httpMock.verify();
   });

   it('should handle error when fetching data', () => {
     const errorResponse = { status: 404, statusText: 'Not Found' };

     spyOn(console, 'error');
     component.serverURL = '/assets/data.json';
     component.ngOnInit();
     const req = httpMock.expectOne('/assets/data.json');
     req.flush(null, errorResponse);

     expect(console.error).toHaveBeenCalledWith(
       'Error loading /assets/data.json',
       jasmine.any(Object) // Use jasmine.any to match any object type
     );
     expect(component.pages).toBeNull();
     httpMock.verify();
   })

   afterEach(() => {
     httpMock.verify();
   });
 });
