import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, switchMap, tap, throwError } from 'rxjs';
import { AuthStore } from './store/auth.repository';
import { AuthService } from './api-gen/services';
import { LoginResult, RefreshToken } from './api-gen/models';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  constructor(private store: AuthStore, private auth: AuthService) {}
  intercept(
    req: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    // Apply the headers
    req = this.reqClone(req);
    console.log('req', req);
    // Also handle errors globally
    return next.handle(req).pipe(
      catchError((err: HttpErrorResponse) => {
        if (
          err.status === 0 &&
          !(
            typeof req.body === 'object' &&
            req.body &&
            'refreshToken' in req.body
          )
        ) {
          return this.tryRefresh().pipe(
            switchMap(() => {
              // Retry the original request with the updated token
              req = this.reqClone(req);
              return next.handle(req);
            }),
            catchError(() => {
              // Handle the error or throw it to propagate it further
              return throwError(err);
            })
          );
        }
        // Handle other errors or throw them to propagate them further
        return throwError(err);
      })
    );
  }

  private reqClone(req: HttpRequest<unknown>): HttpRequest<unknown> {
    const JwtTokenOrNull = this.store.getJwtToken();
    const JwtToken = typeof JwtTokenOrNull == 'string' ? JwtTokenOrNull : '';
    return req.clone({
      setHeaders: {
        Authorization: 'Bearer ' + JwtToken,
      },
    });
  }

  private tryRefresh(): Observable<LoginResult> {
    const refreshToken = this.store.getRefreshToken();
    if (refreshToken) {
      const refreshTokenDto: RefreshToken = {
        username: this.store.getUsername(),
        refreshToken: refreshToken,
      };
      return this.auth.refreshToken({ body: refreshTokenDto }).pipe(
        tap((response: LoginResult) => {
          console.log('Refresh ', response);
          this.store.setToken(response.accessToken ? response.accessToken : '');
          this.store.setRefreshToken(
            response.refreshToken ? response.refreshToken : ''
          );
        }),
        catchError((error: HttpErrorResponse) => {
          console.error('Refresh failed:', error);
          this.store.resetStore();
          return throwError(error);
        })
      );
    }
    return throwError('Refresh token is missing.');
  }
}
