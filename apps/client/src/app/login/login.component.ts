import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../api-gen/services';
import { Login } from '../api-gen/models';
import { AuthStore } from '../store/auth.repository';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(
    private auth: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private store: AuthStore
  ) {}
  isLoggedIn = false;
  loginError = false;
  loginForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });

  ngOnInit() {
    this.isLoggedIn = this.store.isLoggedIn();
    console.log('this.isLoggedIn', this.isLoggedIn);
  }

  login() {
    if (this.loginForm.valid) {
      this.loginError = false;
      const loginData = this.loginForm.value as Login;
      console.log(loginData)
      this.auth.login({ body: loginData }).subscribe(
        (response) => {
          console.log('Logged in!');
          console.log(response);
          this.store.setUsername(
            loginData.email ? loginData.email : null
          );
          this.store.setToken(response.accessToken ? response.accessToken : '');
          this.store.setRefreshToken(
            response.refreshToken ? response.refreshToken : ''
          );
          let returnURL = this.route.snapshot.paramMap.get('returnURL');
          if (returnURL == null || returnURL == '') {
            returnURL = '/';
          }
          this.router.navigate([returnURL]).catch((error) => {
            console.error(error);
          });
        },
        (error) => {
          console.error('Login failed:', error);
          this.loginError = true;
        }
      );
    }
  }

  logout() {
    this.store.resetStore();
    this.isLoggedIn = false;
  }
}
