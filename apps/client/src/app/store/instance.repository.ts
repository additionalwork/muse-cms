import { createStore } from '@ngneat/elf';
import {
  addEntities,
  deleteAllEntities,
  getAllEntities,
  withEntities,
} from '@ngneat/elf-entities';
import { localStorageStrategy, persistState } from '@ngneat/elf-persist-state';

export interface Instance {
  id: string;
  Name: string;
  [key: string]: string;
}

export class InstanceStore {
  _slug: string;
  private instanceStore;
  constructor(slug: string) {
    this._slug = slug;
    this.instanceStore = createStore(
      { name: 'instance' + this._slug },
      withEntities<Instance>()
    );
    persistState(this.instanceStore, {
      key: 'instance_' + slug,
      storage: localStorageStrategy,
    });
  }

  public setInstances(instance: Instance[]) {
    this.instanceStore.update(deleteAllEntities());
    this.instanceStore.update(addEntities(instance));
  }

  public getInstances(): Instance[] {
    return this.instanceStore.query(getAllEntities());
  }
}
