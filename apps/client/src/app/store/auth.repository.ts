import { Injectable } from '@angular/core';
import { createStore, setProps, withProps } from '@ngneat/elf';
import { localStorageStrategy, persistState } from '@ngneat/elf-persist-state';

interface TokenProps {
  jwtToken: string | null;
  refreshToken: string | null;
  username: string | null;
}

@Injectable({
  providedIn: 'root',
})
export class AuthStore {
  constructor() {
    persistState(this.authStore, {
      key: 'auth',
      storage: localStorageStrategy,
    });
  }

  private authStore = createStore(
    { name: 'auth' },
    withProps<TokenProps>({ jwtToken: null,refreshToken:null,username:null }),
  );

  public getJwtToken(): string | null {
    return this.authStore.query(state => state.jwtToken)
  }
  
  public getRefreshToken(): string | null {
    return this.authStore.query(state => state.refreshToken)
  }

  public getUsername(): string | null {
    return this.authStore.query(state => state.username)
  }

  public setToken(jwtToken: TokenProps['jwtToken']) {
    this.authStore.update(setProps({ jwtToken }));
  }

  public setUsername(username: TokenProps['username']) {
    this.authStore.update(setProps({ username }));
  }

  public setRefreshToken(refreshToken: TokenProps['refreshToken']) {
    this.authStore.update(setProps({ refreshToken }));
  }

  public resetStore() {
    this.authStore.reset();
  }

  public isLoggedIn(): boolean {
    return this.authStore.query(state => !!state.jwtToken)
  }
}
