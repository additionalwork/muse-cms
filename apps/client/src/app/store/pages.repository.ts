import { Injectable } from '@angular/core';
import { createStore, setProps, withProps } from '@ngneat/elf';
import { localStorageStrategy, persistState } from '@ngneat/elf-persist-state';
import { Page } from '../main/main.component';

interface PagesProps {
  pages: Page[];
}

@Injectable({
  providedIn: 'root',
})
export class PagesStore {
  constructor() {
    persistState(this.pagesStore, {
      key: 'pages',
      storage: localStorageStrategy,
    });
  }

  private pagesStore = createStore(
    { name: 'pages' },
    withProps<PagesProps>({ pages: [] })
  );
  
  public setPages(pages: PagesProps['pages']) {
    this.pagesStore.update(setProps({ pages }));
  }

  public getPages():Page[] {
    return this.pagesStore.query(state => state.pages)
  }

  public isPagesExist(): boolean {
    return this.pagesStore.query(state => state.pages.length)>0
  }

  public resetStore() {
    this.pagesStore.reset();
  }
}
