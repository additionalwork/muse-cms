import { Injectable } from '@angular/core';
import { createStore } from '@ngneat/elf';
import { addEntities, deleteAllEntities, getAllEntities, getEntityByPredicate, withEntities } from '@ngneat/elf-entities';
import { localStorageStrategy, persistState } from '@ngneat/elf-persist-state';
import { UserType } from '../api-gen/models';

@Injectable({
  providedIn: 'root',
})
export class UserTypeStore {
  constructor() {
    persistState(this.userTypeStore, {
      key: 'usertype',
      storage: localStorageStrategy,
    });
  }

  private userTypeStore = createStore(
    { name: 'usertype' },
    withEntities<UserType>(),
  );

  public clear() {
    this.userTypeStore.update(deleteAllEntities());
  }

  public setUserTypes(userTypes: UserType[]) {
    this.userTypeStore.update(deleteAllEntities());
    this.userTypeStore.update(addEntities(userTypes));
  }

  public addUserType(userTypes: UserType) {
    this.userTypeStore.update(addEntities(userTypes));
  }

  public getUserTypes():UserType[] {
    return this.userTypeStore.query(getAllEntities())
  }

  public getUserTypesBySlug(_slug:string):UserType|undefined {
    return this.userTypeStore.query(getEntityByPredicate(({ slug }) => slug === _slug))
  }
}


