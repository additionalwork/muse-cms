import { Component, OnInit } from '@angular/core';
import { Router, Scroll } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Block } from 'cms-extensions';
import { JsonFileService, UserTypeService } from '../api-gen/services';
import { AuthStore } from '../store/auth.repository';
import { UserTypeStore } from '../store/usertype.repository';
import { Instance, InstanceStore } from '../store/instance.repository';

export interface Page {
  _id: number;
  name: string;
  path: string;
  description: string | undefined;
  blocks: Block[];
  requiresAuth: boolean;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  pages: Page[] = [];
  currentPath = ''; //'/api/v0/index';
  isAuthorized = false;
  currentPage: Page = {
    name: 'index',
    path: '/',
    blocks: [
      {
        tagName: 'div',
        text: 'Приложение находится в режиме обслуживания',
      },
    ],
    _id: 0,
    description: undefined,
    requiresAuth: false,
  };

  constructor(
    private jsonFileService: JsonFileService,
    private userTypeService: UserTypeService,
    private router: Router,
    private authStore: AuthStore,
    private usertypeStore: UserTypeStore
  ) {}

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof Scroll) {
        this.currentPath = event.routerEvent.url;
        console.log('currentPath ', event.routerEvent.url);
      }
    });
    this.isAuthorized = this.authStore.isLoggedIn();
    console.log('isAuthorized ', this.isAuthorized);

    if (this.isAuthorized) {
      this.getPagesAuth();
    } else {
      this.getPages();
    }
    this.getUserTypes()
  }

  setPages(pages: Page[]) {
    this.pages = pages;
    console.log('pages ', this.pages);
    this.setCurrentPage();
  }

  setCurrentPage() {
    console.log('setCurrentPage');
    let wasFound = false;
    this.pages.forEach((p) => {
      if (p.path == this.currentPath) {
        this.currentPage = p;
        console.log('isRequire ', p.requiresAuth);
        if (!this.isAuthorized && this.currentPage.requiresAuth) {
          this.router
            .navigate(['/login', { returnURL: this.currentPath }])
            .catch((error) => {
              console.error(error);
            });
        }
        wasFound = true;
      }
    });
    if (!wasFound && this.pages.length > 0) {
      this.currentPage.blocks = [
        {
          tagName: 'div',
          text: 'Страница с заданным маршрутом не была найдена. Список всех маршрутов',
        },
        {
          tagName: 'ul',
          childNodes: this.pages
            .map((p) => {
              return {
                tagName: 'a',
                attributes: { href: p.path },
                text: p.name,
              };
            })
            .map((a) => {
              return { tagName: 'li', childNodes: [a] };
            }),
        },
      ];
    }
  }

  getPages() {
    this.jsonFileService.getJsonFileNotAuth().subscribe(
      (data) => {
        this.setPages(JSON.parse(data) as Page[]);
        console.log('getPages ', JSON.parse(data) as Page[]);
      },
      (error: HttpErrorResponse) => {
        console.error('Error loading JsonFile', error);
      }
    );
  }

  getPagesAuth() {
    this.jsonFileService.getJsonFile().subscribe(
      (data) => {
        this.setPages(JSON.parse(data) as Page[]);
        this.isAuthorized = true;
        console.log('getPagesAuth ', JSON.parse(data) as Page[]);
      },
      (error: HttpErrorResponse) => {
        console.error('Error loading JsonFile/auth', error);
        // if the token has gone bad, we get the pages as an unauthorized user
        this.isAuthorized = false;
        this.getPages();
      }
    );
  }

  getUserTypes() {
    this.userTypeService.getUserTypes().subscribe(
      (data) => {
        console.log('getUserTypes ', data);
        this.usertypeStore.clear()
        data.forEach(el => {
          this.usertypeStore.addUserType( el.scheme);
          const instStore = new InstanceStore(el.scheme.slug);
          const updatedInstances: Instance[] = el.instancesRows?.map((instance) => ({
            ...instance, // Копируем все свойства из исходного объекта
            id: instance['Id'] as string, // Преобразуем свойство Id в id
          })) as Instance[];
          console.log('el.instancesRows as Instance[] ', updatedInstances);
          instStore.setInstances(updatedInstances)
        })
      },
      (error: HttpErrorResponse) => {
        console.error('Error loading UserTypes', error);
      }
    );
  }
}
