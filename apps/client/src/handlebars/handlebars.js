/* eslint-disable */
module.exports = function (handlebars) {
    handlebars.registerHelper('eq', (a, b) => a === b);
    handlebars.registerHelper('includes', (a, b) => a && b && a.toLowerCase().includes(b.toLowerCase()));
    handlebars.registerHelper('hasPatchOperations', (operations) => operations.some(o => o.method == 'patch'));
};
