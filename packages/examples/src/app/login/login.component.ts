import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  constructor(private http: HttpClient,private route: ActivatedRoute, private router: Router) {}
  loginForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });
  @Output() onLogged = new EventEmitter<string>();

  login() {
    if (this.loginForm.valid) {
      const loginData = {
        username: this.loginForm.value.email,
        password: this.loginForm.value.password,
      };

      this.http.post(environment.apiUrl+'auth', loginData).subscribe(
        (response: any) => {
          console.log('Logged in!');
          console.log(response);
          this.onLogged.emit(response.token);
          localStorage.setItem('jwtToken', response.token);
          const returnURL = this.route.snapshot.paramMap.get('returnURL');
          this.router.navigate([returnURL]);
        },
        (error) => {
          console.error('Login failed:', error);
        }
      );
    }
  }
}
