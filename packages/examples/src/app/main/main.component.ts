import { Component, OnInit } from '@angular/core';
import { Router, Scroll } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Block } from 'cms-extensions';

export interface Page {
  _id: number;
  name: string;
  path: string;
  description: string | undefined;
  blocks: Block[];
  requiresAuth: boolean;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  pages: Page[] = [];
  currentPath = ''; //'/api/v0/index';
  isAuthorized = false;
  currentPage: Page = {
    name: 'index',
    path: '/',
    blocks: [
      {
        tagName: 'div',
        text: 'Приложение находится в режиме обслуживания',
      },
    ],
    _id: 0,
    description: undefined,
    requiresAuth: false,
  };

  constructor(private http: HttpClient, private router: Router) {}

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof Scroll) {
        this.currentPath = event.routerEvent.url;
        console.log('currentPath ', event.routerEvent.url);
      }
    });
    const jwtToken = localStorage.getItem('jwtToken');
    if (jwtToken !== null && jwtToken != '') {
      this.isAuthorized = true;
      this.getPagesAuth(jwtToken);
    } else {
      this.getPages();
    }
    console.log('isAuthorized ', this.isAuthorized);
  }

  setPages(pages: Page[]) {
    this.pages = pages;
    console.log('pages ', this.pages);
    this.setCurrentPage();
  }

  setCurrentPage() {
    console.log('setCurrentPage');
    let wasFound = false;
    this.pages.forEach((p) => {
      if (p.path == this.currentPath) {
        this.currentPage = p;
        console.log('isRequire ', p.requiresAuth);
        if (!this.isAuthorized) {
          this.router.navigate(['/login', { returnURL: this.currentPath }]);
        }
        wasFound = true;
      }
    });
    if (!wasFound && this.pages.length > 0) {
      this.currentPage.blocks = [
        {
          tagName: 'div',
          text: 'Страница с заданным маршрутом не была найдена. Список всех маршрутов',
        },
        {
          tagName: 'ul',
          childNodes: this.pages
            .map((p) => {
              return {
                tagName: 'a',
                attributes: { href: p.path },
                text: p.name,
              };
            })
            .map((a) => {
              return { tagName: 'li', childNodes: [a] };
            }),
        },
      ];
    }
  }

  getPages() {
    this.http.get(environment.apiUrl + 'JsonFile').subscribe(
      (data) => {
        this.setPages(data as Page[]);
      },
      (error: HttpErrorResponse) => {
        console.error(
          'Error loading ' + environment.apiUrl + 'JsonFile',
          error
        );
      }
    );
  }

  getPagesAuth(jwtToken: string) {
    this.http
      .get(environment.apiUrl + 'JsonFile/auth', {
        headers: {
          Authorization: 'Bearer ' + jwtToken,
        },
      })
      .subscribe(
        (data) => {
          this.setPages(data as Page[]);
          this.isAuthorized = true;
        },
        (error: HttpErrorResponse) => {
          console.error(
            'Error loading ' + environment.apiUrl + 'JsonFile/auth',
            error
          );
          // if the token has gone bad, we get the pages as an unauthorized user
          this.isAuthorized = false;
          this.getPages();
        }
      );
  }
}
