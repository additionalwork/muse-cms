# CmsExtensions

<!-- This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.0.0. -->
This library provides the ability to dynamically build HTML markup and embed it on a web page. The data for the library is passed in JSON format.
## Installation

Coming soon

## Build

Run `ng build cms-extensions` to build the project. The build artifacts will be stored in the `dist/` directory.

## Usage

After building you can import module into `app.module.ts`

```ts
import { MybuilderModule } from 'cms-extensions';
@NgModule({
  imports: [ MybuilderModule ],
})
export class AppModule { }
```

The application `test-app` with an example of using the library is located in the folder `examples`.

## Tests

Run `ng test cms-extensions` to execute the unit tests via [Karma](https://karma-runner.github.io).
To create a test coverage report run `ng test cms-extensions --code-coverage`. The report will be stored in the `coverage/` directiory.
To configure the tests in detail, use the configuration file `karma.conf.js`.

<!-- ## Code scaffolding

Run `ng generate component component-name --project cms-extensions` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project cms-extensions`.
> Note: Don't forget to add `--project cms-extensions` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build cms-extensions` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build cms-extensions`, go to the dist folder `cd dist/cms-extensions` and run `npm publish`.

## Running unit tests

Run `ng test cms-extensions` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page. -->
