import { TestBed } from '@angular/core/testing';

import { MybuilderService } from './mybuilder.service';

describe('MybuilderService', () => {
  let service: MybuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MybuilderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
