/* eslint-disable no-console */

import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import {MatListModule} from '@angular/material/list';

//import { Block, MyBuilderComponent } from 'cms-extensions';
import { Block, MyBuilderComponent } from './mybuilder.component';
import { By } from '@angular/platform-browser';

describe('MyBuilderComponent', () => {
  let component: MyBuilderComponent;
  let fixture: ComponentFixture<MyBuilderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatListModule],
      declarations: [MyBuilderComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should set myJson property correctly', () => {
    const myJson = { tagName: 'div' };
    component.myJson = [myJson];
    fixture.detectChanges();
    expect(component.myJson).toEqual([myJson]);
  });

  it('should declare ngAfterViewInit method', () => {
    expect(component.ngAfterViewInit).toBeTruthy();
  });

  // должен загрузить компонент и создать элементы DOM
  it('should load component and create DOM elements', () => {
    const myjson: Block[] = [
      {
        tagName: 'div',
        childNodes: [
          {
            tagName: 'p',
            attributes: {
              id: 'myID',
              class: 'myClass',
            },
            text: 'text',
          },
          {
            tagName: 'h1',
          },
        ],
      },
    ];
    component.myJson = myjson;

    expect(myjson).toBeDefined();
    expect(Array.isArray(myjson)).toBe(true);
    const childNode = myjson[0];
    expect(childNode.tagName).toBe('div');
    expect(childNode.text).toBeUndefined();

    if (childNode.childNodes) {
      const nestedChildNode = childNode.childNodes[0];
      expect(nestedChildNode.tagName).toBe('p');
      expect(nestedChildNode.attributes).toBeDefined();
      expect(typeof nestedChildNode.attributes === 'object').toBe(true);
      expect(nestedChildNode.attributes?.['id']).toBe('myID');
      expect(nestedChildNode.attributes?.['class']).toBe('myClass');
      expect(nestedChildNode.text).toBe('text');

      const nestedChildNodeWithoutAttributes = childNode.childNodes[1];
      expect(nestedChildNodeWithoutAttributes.tagName).toBe('h1');
      expect(nestedChildNodeWithoutAttributes.attributes).toBeUndefined();
      expect(nestedChildNodeWithoutAttributes.text).toBeUndefined();
    }
  });

  // должен обрабатывать отложенное создание компонента
  it('should handle delayed component creation', fakeAsync(() => {
    const myJson: Block[] = [
      {
        tagName: 'div',
      },
    ];

    component.myJson = myJson;

    tick(1000);

    fixture.detectChanges();

    const divElement = fixture.nativeElement.querySelector('div');
    expect(divElement).toBeDefined();
  }));

  it('should handle empty myJson object', () => {
    const myjson: Block[] = [];

    component.myJson = myjson;
    fixture.detectChanges();

    // Проверка отсутствия созданных элементов DOM
    const parentElement: HTMLElement = fixture.nativeElement;
    const childElements = parentElement.querySelectorAll('*');
    expect(childElements.length).toBe(1); // Родительский элемент
  });

  it('should handle missing childNodes property', () => {
    const myjson: Block[] = [
      {
        tagName: 'p',
      },
    ];

    component.myJson = myjson;
    fixture.detectChanges();

    // Проверка отсутствия дочерних элементов
    const parentElement: HTMLElement = fixture.nativeElement;
    const childElements = parentElement.querySelectorAll('p');
    expect(childElements.length).toBe(1); // Родительский элемент
  });

  //должны эффективно обрабатывать большие объемы данных
  it('should handle large volume of data efficiently', () => {
    const myjson: Block[] = [
      {
        tagName: 'div',
        childNodes: [
          {
            tagName: 'p',
            attributes: {
              id: 'p',
              class: 'p',
            },
            text: 'text',
          },
          {
            tagName: 'a',
            attributes: {
              id: 'a',
              class: 'a',
            },
          },
          {
            tagName: 'h1',
            attributes: {
              id: 'h1',
              class: 'h1',
            },
          },
          {
            tagName: 'h2',
            attributes: {
              id: 'h2',
              class: 'h2',
            },
          },
          {
            tagName: 'h3',
            attributes: {
              id: 'h3',
              class: 'h3',
            },
          },
          {
            tagName: 'h4',
            attributes: {
              id: 'h4',
              class: 'h4',
            },
          },
          {
            tagName: 'h5',
            attributes: {
              id: 'h5',
              class: 'h5',
            },
          },
          {
            tagName: 'h6',
            attributes: {
              id: 'h6',
              class: 'h6',
            },
          },
        ],
      },
    ];
    component.myJson = myjson;

    const startTime = performance.now();

    fixture.detectChanges();

    const endTime = performance.now();
    const executionTime = endTime - startTime;

    console.log(`Execution time: ${executionTime} milliseconds`);

    expect(executionTime).toBeLessThan(1000);
  });

  it('should update the DOM when myJson input changes', fakeAsync(() => {
    const initialMyJson: Block[] = [
      {
        tagName: 'div',
      },
    ];
    component.myJson = initialMyJson;
    fixture.detectChanges();

    //Check the initial state of the DOM
    const divElement = fixture.nativeElement.querySelector('div');
    expect(divElement).toBeTruthy();

    //Update myJson
    const updatedMyJson: Block[] = [
        {
          tagName: 'div',
          childNodes: [
            {
              tagName: 'p',
              attributes: {
                id: 'myID',
                class: 'myClass',
              },
              text: 'text',
            },
          ],
        },
    ];

    component.myJson = updatedMyJson;
    fixture.detectChanges();
    tick();

    const updatedDivElement = fixture.nativeElement.querySelector('div');
    const pElement = fixture.nativeElement.querySelector('p');
    expect(updatedDivElement).toBeTruthy();
    expect(pElement).toBeTruthy();
    expect(pElement.textContent).toBe('text');
    expect(pElement.getAttribute('id')).toBe('myID');
    expect(pElement.getAttribute('class')).toBe('myClass');
  }));

  xit('should handle invalid myJson input', fakeAsync(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const invalidMyJson: any = 'invalidJson';

    component.myJson = invalidMyJson;
    fixture.detectChanges();
    tick();

    const parentElement: HTMLElement = fixture.nativeElement;
    const childElements = parentElement.querySelectorAll('*');
    expect(childElements.length).toBe(0);
  }));

  it('should handle invalid values in myJson input', fakeAsync(() => {
    const invalidJson: Block[] = [
      {
        tagName: 'div',
        text: '',
      },
      {
        tagName: 'p',
        text: '123',
      },
    ];
    component.myJson = invalidJson;
    fixture.detectChanges();
    tick();

    const divElement = fixture.nativeElement.querySelector('div').firstElementChild;
    expect(divElement.innerHTML).toBe('');

    const pElement = fixture.nativeElement.querySelector('p');
    expect(pElement).toBeDefined();
    expect(pElement.innerHTML).toBe('123');
  }));

  it('should create text nodes if there are no matching components', () => {
    const myJson: Block[] = [
      {
        tagName: 'div',
        childNodes: [
          {
            tagName: 'unknown',
            text: 'Some text',
          },
        ],
      },
    ];

    component.myJson = myJson;
    component.loadComponent();

    const createdDiv = fixture.nativeElement.querySelector('div');
    expect(createdDiv).toBeTruthy();

    const createdUnknownNode = createdDiv.childNodes[0];
    expect(createdUnknownNode).toBeTruthy();
    expect(createdUnknownNode.nodeType).toEqual(1);
    expect(createdUnknownNode.textContent).toEqual('Some text');
  });

  it('should handle nested child nodes', () => {
    const myJson: Block[] = [
      {
        tagName: 'div',
        childNodes: [
          {
            tagName: 'p',
            childNodes: [
              {
                tagName: 'span',
                text: 'text',
              },
            ],
          },
        ],
      },
    ];
    component.myJson = myJson;
    component.loadComponent();

    const createdDiv = fixture.nativeElement.querySelector('div');
    expect(createdDiv).toBeTruthy();

    const createdParagraph = fixture.nativeElement.querySelector('div p');
    expect(createdParagraph).toBeTruthy();

    const createdSpan = fixture.nativeElement.querySelector('div p span');
    expect(createdSpan).toBeTruthy();
    expect(createdSpan.textContent).toEqual('text');
  });

  it('should handle special characters or HTML escaping in myJson input', () => {
    const myJson: Block[] = [
        {
          tagName: 'div',
          childNodes: [
            {
              tagName: 'p',
              attributes: {
                id: 'myID',
                class: 'myClass',
              },
              text: 'Special characters: < > &',
            },
          ],
        },
    ];

    component.myJson = myJson;
    fixture.detectChanges();

    const pElement = fixture.nativeElement.querySelector('p');
    expect(pElement).toBeTruthy();
    expect(pElement.innerHTML).toBe('Special characters: &lt; &gt; &amp;');
  });

  it('should create MatList elements', () => {
    const myjson: Block[] = [
      {
        tagName: 'mat-list',
        childNodes: [
          {
            tagName: 'mat-list-item',
            attributes: {
              class: 'mat-list-item',
              'role': 'listitem',
            },
            text: 'Item 1',            
          },
        ],
        attributes: {
          class: 'mat-list',
          'role': 'list',
        },
      },
    ];
    component.myJson = myjson;

    expect(myjson).toBeDefined();

    const matListComponent = fixture.nativeElement.querySelector('mat-list');
    expect(matListComponent).toBeTruthy();

    const matListItem = matListComponent.childNodes[0];
    expect(matListItem).toBeTruthy();
    expect(matListItem.nodeType).toEqual(1);
    expect(matListItem.textContent).toEqual('Item 1');
  });
  
});
