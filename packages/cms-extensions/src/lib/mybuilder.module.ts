import { NgModule } from '@angular/core';
import { MyBuilderComponent } from './mybuilder.component';



@NgModule({
  declarations: [
    MyBuilderComponent
  ],
  imports: [
  ],
  exports: [
    MyBuilderComponent
  ]
})
export class MybuilderModule { }
